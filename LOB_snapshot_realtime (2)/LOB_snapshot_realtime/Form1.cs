﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using Newtonsoft.Json;

namespace LOB_snapshot_realtime
{
    public partial class Form1 : Form
    {
        private UdpClient client;
        public Dictionary<string, string> realtime_datatype;
        public Dictionary<string, string> realtime_datatype2;

        public Form1()
        {
            InitializeComponent();
            this.client = new UdpClient();
            axKFOpenAPI1.CommConnect(1);

            realtime_datatype = new Dictionary<string, string>();
            realtime_datatype2 = new Dictionary<string, string>();

            realtime_datatype["매도수량5"] = "65";
            realtime_datatype["매도호가5"] = "45";
            realtime_datatype["매도건수5"] = "105";

            realtime_datatype["매도수량4"] = "64";
            realtime_datatype["매도호가4"] = "44";
            realtime_datatype["매도건수4"] = "104";

            realtime_datatype["매도수량3"] = "63";
            realtime_datatype["매도호가3"] = "43";
            realtime_datatype["매도건수3"] = "103";

            realtime_datatype["매도수량2"] = "62";
            realtime_datatype["매도호가2"] = "42";
            realtime_datatype["매도건수2"] = "102";

            realtime_datatype["매도수량1"] = "61";
            realtime_datatype["매도호가1"] = "41";
            realtime_datatype["매도건수1"] = "101";

            realtime_datatype["매수수량5"] = "75";
            realtime_datatype["매수호가5"] = "55";
            realtime_datatype["매수건수5"] = "115";

            realtime_datatype["매수수량4"] = "74";
            realtime_datatype["매수호가4"] = "54";
            realtime_datatype["매수건수4"] = "114";

            realtime_datatype["매수수량3"] = "73";
            realtime_datatype["매수호가3"] = "53";
            realtime_datatype["매수건수3"] = "113";

            realtime_datatype["매수수량2"] = "72";
            realtime_datatype["매수호가2"] = "52";
            realtime_datatype["매수건수2"] = "112";

            realtime_datatype["매수수량1"] = "71";
            realtime_datatype["매수호가1"] = "51";
            realtime_datatype["매수건수1"] = "111";

            realtime_datatype["매도호가총잔량"] = "121";
            realtime_datatype["매수호가총잔량"] = "125";
            realtime_datatype["호가시간"] = "21";

            realtime_datatype2["체결시간"] = "20";
            realtime_datatype2["현재가"] = "10";
            realtime_datatype2["대비기호"] = "25";
            realtime_datatype2["전일대비"] = "11";
            realtime_datatype2["등락율"] = "12";
            realtime_datatype2["체결량"] = "15";
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {

        }

        private void axKFOpenAPI1_OnReceiveTrData(object sender, AxKFOpenAPILib._DKFOpenAPIEvents_OnReceiveTrDataEvent e)
        {

        }

        private void axKFOpenAPI1_OnReceiveRealData(object sender, AxKFOpenAPILib._DKFOpenAPIEvents_OnReceiveRealDataEvent e)
        {
            string output = "";

            Dictionary<string, string> output_dict = new Dictionary<string, string>();
            if ((e.sRealType.Trim() == "해외선물시세") || (e.sRealType.Trim() == "해외옵션시세"))
            {
                output_dict["type"] = "contract";
                output_dict["시간"] = DateTime.Now.ToString("HH:mm:ss.ffffff");
                output_dict["체결시간"] = axKFOpenAPI1.GetCommRealData(e.sJongmokCode, int.Parse(realtime_datatype2["체결시간"]));
                output_dict["현재가"] = axKFOpenAPI1.GetCommRealData(e.sJongmokCode, int.Parse(realtime_datatype2["현재가"]));
                output_dict["대비기호"] = axKFOpenAPI1.GetCommRealData(e.sJongmokCode, int.Parse(realtime_datatype2["대비기호"]));
                output_dict["체결량"] = axKFOpenAPI1.GetCommRealData(e.sJongmokCode, int.Parse(realtime_datatype2["체결량"]));
                output = JsonConvert.SerializeObject(output_dict, Formatting.None);
                this.client.Send(Encoding.UTF8.GetBytes(output), Encoding.UTF8.GetBytes(output).Length, "127.0.0.1", 7777);
            }

            else if ((e.sRealType.Trim() == "해외선물호가") || (e.sRealType.Trim() == "해외옵션호가"))
            {
                output_dict["type"] = "lob";
                output_dict["시간"] = DateTime.Now.ToString("HH:mm:ss.ffffff");
                output_dict["매도수량5"] = axKFOpenAPI1.GetCommRealData(e.sJongmokCode, int.Parse(realtime_datatype["매도수량5"]));
                output_dict["매도호가5"] = axKFOpenAPI1.GetCommRealData(e.sJongmokCode, int.Parse(realtime_datatype["매도호가5"]));
                output_dict["매도건수5"] = axKFOpenAPI1.GetCommRealData(e.sJongmokCode, int.Parse(realtime_datatype["매도건수5"]));

                output_dict["매도수량4"] = axKFOpenAPI1.GetCommRealData(e.sJongmokCode, int.Parse(realtime_datatype["매도수량4"]));
                output_dict["매도호가4"] = axKFOpenAPI1.GetCommRealData(e.sJongmokCode, int.Parse(realtime_datatype["매도호가4"]));
                output_dict["매도건수4"] = axKFOpenAPI1.GetCommRealData(e.sJongmokCode, int.Parse(realtime_datatype["매도건수4"]));

                output_dict["매도수량3"] = axKFOpenAPI1.GetCommRealData(e.sJongmokCode, int.Parse(realtime_datatype["매도수량3"]));
                output_dict["매도호가3"] = axKFOpenAPI1.GetCommRealData(e.sJongmokCode, int.Parse(realtime_datatype["매도호가3"]));
                output_dict["매도건수3"] = axKFOpenAPI1.GetCommRealData(e.sJongmokCode, int.Parse(realtime_datatype["매도건수3"]));

                output_dict["매도수량2"] = axKFOpenAPI1.GetCommRealData(e.sJongmokCode, int.Parse(realtime_datatype["매도수량2"]));
                output_dict["매도호가2"] = axKFOpenAPI1.GetCommRealData(e.sJongmokCode, int.Parse(realtime_datatype["매도호가2"]));
                output_dict["매도건수2"] = axKFOpenAPI1.GetCommRealData(e.sJongmokCode, int.Parse(realtime_datatype["매도건수2"]));

                output_dict["매도수량1"] = axKFOpenAPI1.GetCommRealData(e.sJongmokCode, int.Parse(realtime_datatype["매도수량1"]));
                output_dict["매도호가1"] = axKFOpenAPI1.GetCommRealData(e.sJongmokCode, int.Parse(realtime_datatype["매도호가1"]));
                output_dict["매도건수1"] = axKFOpenAPI1.GetCommRealData(e.sJongmokCode, int.Parse(realtime_datatype["매도건수1"]));

                output_dict["매수수량5"] = axKFOpenAPI1.GetCommRealData(e.sJongmokCode, int.Parse(realtime_datatype["매수수량5"]));
                output_dict["매수호가5"] = axKFOpenAPI1.GetCommRealData(e.sJongmokCode, int.Parse(realtime_datatype["매수호가5"]));
                output_dict["매수건수5"] = axKFOpenAPI1.GetCommRealData(e.sJongmokCode, int.Parse(realtime_datatype["매수건수5"]));

                output_dict["매수수량4"] = axKFOpenAPI1.GetCommRealData(e.sJongmokCode, int.Parse(realtime_datatype["매수수량4"]));
                output_dict["매수호가4"] = axKFOpenAPI1.GetCommRealData(e.sJongmokCode, int.Parse(realtime_datatype["매수호가4"]));
                output_dict["매수건수4"] = axKFOpenAPI1.GetCommRealData(e.sJongmokCode, int.Parse(realtime_datatype["매수건수4"]));

                output_dict["매수수량3"] = axKFOpenAPI1.GetCommRealData(e.sJongmokCode, int.Parse(realtime_datatype["매수수량3"]));
                output_dict["매수호가3"] = axKFOpenAPI1.GetCommRealData(e.sJongmokCode, int.Parse(realtime_datatype["매수호가3"]));
                output_dict["매수건수3"] = axKFOpenAPI1.GetCommRealData(e.sJongmokCode, int.Parse(realtime_datatype["매수건수3"]));

                output_dict["매수수량2"] = axKFOpenAPI1.GetCommRealData(e.sJongmokCode, int.Parse(realtime_datatype["매수수량2"]));
                output_dict["매수호가2"] = axKFOpenAPI1.GetCommRealData(e.sJongmokCode, int.Parse(realtime_datatype["매수호가2"]));
                output_dict["매수건수2"] = axKFOpenAPI1.GetCommRealData(e.sJongmokCode, int.Parse(realtime_datatype["매수건수2"]));

                output_dict["매수수량1"] = axKFOpenAPI1.GetCommRealData(e.sJongmokCode, int.Parse(realtime_datatype["매수수량1"]));
                output_dict["매수호가1"] = axKFOpenAPI1.GetCommRealData(e.sJongmokCode, int.Parse(realtime_datatype["매수호가1"]));
                output_dict["매수건수1"] = axKFOpenAPI1.GetCommRealData(e.sJongmokCode, int.Parse(realtime_datatype["매수건수1"]));

                output_dict["매수호가총잔량"] = axKFOpenAPI1.GetCommRealData(e.sJongmokCode, int.Parse(realtime_datatype["매수호가총잔량"]));
                output_dict["매도호가총잔량"] = axKFOpenAPI1.GetCommRealData(e.sJongmokCode, int.Parse(realtime_datatype["매도호가총잔량"]));


                output = JsonConvert.SerializeObject(output_dict, Formatting.None);
                this.client.Send(Encoding.UTF8.GetBytes(output), Encoding.UTF8.GetBytes(output).Length, "127.0.0.1", 7777);
                //redis.addList("current_hoga_" + database_text.Text, output);
            }
        }

        private void set_btn_Click(object sender, EventArgs e)
        {
            string strRQName = "선물옵션현재가";
            string strTRCode = Constants.TR_OPT10001;
            axKFOpenAPI1.SetInputValue("종목코드", code_text.Text);

            int iRet = axKFOpenAPI1.CommRqData(strRQName, strTRCode, "", "13");
            if (iRet == 0) { }
        }


        public static class Constants
        {
            public const string TR_OPT10001 = "opt10001"; //현재가
            public const string TR_OPT10003 = "opt10005"; //관심종목
            public const string TR_OPC10001 = "opc10001"; //틱차트
            public const string TR_OPC10002 = "opc10002"; //분차트
            public const string TR_OPC10003 = "opc10003"; //일차트
            public const string TR_OPC10004 = "opc10004";//주차트
            public const string TR_OPC10005 = "opc10005"; //월차트
            public const string TR_OPW30005 = "opw30005"; //주문체결
            public const string TR_OPW30003 = "opw30003"; //잔고내역

            public const int OP_ERR_NONE = 0; //"정상처리" 
            public const int OP_ERR_NO_LOGIN = -1; //"미접속상태"	
            public const int OP_ERR_CONNECT = -101; //"서버 접속 실패" 
            public const int OP_ERR_VERSION = -102; //"버전처리가 실패하였습니다. 
            public const int OP_ERR_TRCODE = -103; //"TrCode가 존재하지 않습니다.”
            public const int OP_ERR_SISE_OVERFLOW = -200; //”시세과부하”
            public const int OP_ERR_ORDER_OVERFLOW = -201; //”주문과부하” 
            public const int OP_ERR_RQ_WRONG_INPUT = -202; //”조회입력값 오류”
            public const int OP_ERR_ORD_WRONG_INPUT = -300; //”주문입력값 오류”
            public const int OP_ERR_ORD_WRONG_ACCPWD = -301; //”계좌비밀번호를 입력하십시오.”
            public const int OP_ERR_ORD_WRONG_ACCNO = -302; //”타인 계좌를 사용할 수 없습니다.”
            public const int OP_ERR_ORD_WRONG_QTY200 = -303; //”경고-주문수량 200개 초과”
            public const int OP_ERR_ORD_WRONG_QTY400 = -304; //”제한-주문수량 400개 초과.”

            public const int DT_NONE = 0;		// 기본문자 형식
            public const int DT_DATE = 1;		// 일자 형식
            public const int DT_TIME = 2;		// 시간 형식
            public const int DT_NUMBER = 3;		// 콤마 숫자 형식
            public const int DT_ZERO_NUMBER = 4;		// 콤마 숫자(0표시) 형식
            public const int DT_SIGN = 5;		// 대비기호 형식
            public const int DT_NUMBER_NOCOMMA = 6;		// 콤마없는 숫자 형식
            public const int DT_ORDREG_GB = 7;		// 접수구분(1:접수, 2:확인, 3:체결, X:거부)
            public const int DT_ORDTYPE = 8;		// 주문유형(1:시장가, 2:지정가, 3:STOP, 4:STOPLIMIT)
            public const int DT_DOUBLE = 9;		// 실수값 처리( 소수점 2자리까지)
            public const int DT_ORDGUBUN = 10;		// 주문구분(1:매도, 2:매수)
            public const int DT_PRICE = 11;		// 가격표시

            public const int DT_LEFT = 16;
            public const int DT_RIGHT = 64;
            public const int DT_CENTER = 32;

            public const int UM_SCREEN_CLOSE = 1000;

        }
    }
}
