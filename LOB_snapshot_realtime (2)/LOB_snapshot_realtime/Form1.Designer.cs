﻿namespace LOB_snapshot_realtime
{
    partial class Form1
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다.
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.axKFOpenAPI1 = new AxKFOpenAPILib.AxKFOpenAPI();
            this.code_text = new System.Windows.Forms.TextBox();
            this.set_btn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.axKFOpenAPI1)).BeginInit();
            this.SuspendLayout();
            // 
            // axKFOpenAPI1
            // 
            this.axKFOpenAPI1.Enabled = true;
            this.axKFOpenAPI1.Location = new System.Drawing.Point(172, 200);
            this.axKFOpenAPI1.Name = "axKFOpenAPI1";
            this.axKFOpenAPI1.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axKFOpenAPI1.OcxState")));
            this.axKFOpenAPI1.Size = new System.Drawing.Size(100, 50);
            this.axKFOpenAPI1.TabIndex = 0;
            this.axKFOpenAPI1.OnReceiveTrData += new AxKFOpenAPILib._DKFOpenAPIEvents_OnReceiveTrDataEventHandler(this.axKFOpenAPI1_OnReceiveTrData);
            this.axKFOpenAPI1.OnReceiveRealData += new AxKFOpenAPILib._DKFOpenAPIEvents_OnReceiveRealDataEventHandler(this.axKFOpenAPI1_OnReceiveRealData);
            // 
            // code_text
            // 
            this.code_text.Location = new System.Drawing.Point(12, 12);
            this.code_text.Name = "code_text";
            this.code_text.Size = new System.Drawing.Size(260, 21);
            this.code_text.TabIndex = 1;
            // 
            // set_btn
            // 
            this.set_btn.Location = new System.Drawing.Point(12, 39);
            this.set_btn.Name = "set_btn";
            this.set_btn.Size = new System.Drawing.Size(260, 23);
            this.set_btn.TabIndex = 2;
            this.set_btn.Text = "설정";
            this.set_btn.UseVisualStyleBackColor = true;
            this.set_btn.Click += new System.EventHandler(this.set_btn_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 71);
            this.Controls.Add(this.set_btn);
            this.Controls.Add(this.code_text);
            this.Controls.Add(this.axKFOpenAPI1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Form1";
            this.Text = "실시간 데이터 수집기";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.axKFOpenAPI1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private AxKFOpenAPILib.AxKFOpenAPI axKFOpenAPI1;
        private System.Windows.Forms.TextBox code_text;
        private System.Windows.Forms.Button set_btn;
    }
}

