﻿namespace lob_realtime_analyze
{
    partial class start_form
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다.
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
            this.recv_data_count_label = new System.Windows.Forms.Label();
            this.VPIN_form_show_btn = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // recv_data_count_label
            // 
            this.recv_data_count_label.AutoSize = true;
            this.recv_data_count_label.Location = new System.Drawing.Point(12, 501);
            this.recv_data_count_label.Name = "recv_data_count_label";
            this.recv_data_count_label.Size = new System.Drawing.Size(97, 12);
            this.recv_data_count_label.TabIndex = 0;
            this.recv_data_count_label.Text = "수신 데이터 수 : ";
            // 
            // VPIN_form_show_btn
            // 
            this.VPIN_form_show_btn.Location = new System.Drawing.Point(14, 12);
            this.VPIN_form_show_btn.Name = "VPIN_form_show_btn";
            this.VPIN_form_show_btn.Size = new System.Drawing.Size(222, 23);
            this.VPIN_form_show_btn.TabIndex = 2;
            this.VPIN_form_show_btn.Text = "VPIN 측정";
            this.VPIN_form_show_btn.UseVisualStyleBackColor = true;
            this.VPIN_form_show_btn.Click += new System.EventHandler(this.VPIN_form_show_btn_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(14, 41);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(222, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "호가 잔량 상관관계";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // start_form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(248, 522);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.VPIN_form_show_btn);
            this.Controls.Add(this.recv_data_count_label);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "start_form";
            this.Text = "Main Server";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label recv_data_count_label;
        private System.Windows.Forms.Button VPIN_form_show_btn;
        private System.Windows.Forms.Button button1;
    }
}

