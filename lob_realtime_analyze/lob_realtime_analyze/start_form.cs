﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Sockets;
using System.Net;
using System.Threading;

namespace lob_realtime_analyze
{
    public partial class start_form : Form
    {
        private UdpClient udp_srv;
        private IPEndPoint remoteEP;

        private List<List<string>> tick_data;
        private List<List<string>> contract_data;

        private int recv_count;
        private vpin_form vpin_form_obj;

        public start_form()
        {
            InitializeComponent();
            this.udp_srv = new UdpClient(7777);
            this.remoteEP = new IPEndPoint(IPAddress.Any, 0);

            this.recv_count = 0;
            this.tick_data = new List<List<string>>();
            this.contract_data = new List<List<string>>();
        }

        public void recvRealtimeData()
        {
            while(true)
            {
                byte[] dgram = this.udp_srv.Receive(ref this.remoteEP);
                string output = Encoding.UTF8.GetString(dgram);
                this.recv_count++;
                this.vpin_form_obj.recvRealtimeData()
                recv_data_count_label.Text = "받은 데이터 수 : " + this.recv_count.ToString();
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Thread t1 = new Thread(new ThreadStart(recvRealtimeData));
            t1.Start();
        }

        private void VPIN_form_show_btn_Click(object sender, EventArgs e)
        {
            this.vpin_form_obj = new vpin_form();
            this.vpin_form_obj.Show();
        }
    }
}
