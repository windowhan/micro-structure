﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lob_realtime_analyze
{
    class VPIN_model
    {
        private int bucket_size;
        private List<List<string>> bucket_data;
        private int tick_count;

        public VPIN_model()
        {
            this.tick_count = 0;
            this.bucket_data = new List<List<string>>();
        }

        public void setBucketSize(int size)
        {
            this.bucket_size = size;
        }

        public List<List<string>> getBucketInfo()
        {
            List<List<string>> result = new List<List<string>>();
            double bucket_high = 0, bucket_low = 0, bucket_open = 0, bucket_close = 0;
            double price = 0;
            int buy_volume = 0, sell_volume = 0;
            string volume_type = "";

            for (int i = 0; i < tick_count; i++)
            {
                price = double.Parse(bucket_data[i][1]);
                volume_type = bucket_data[i][2];

                if (i == 0 || i % bucket_size == 0)
                {
                    bucket_open = price;
                    bucket_high = 0;
                    bucket_low = 0;
                    bucket_close = 0;

                    buy_volume = 0;
                    sell_volume = 0;
                }

                if (i % bucket_size == (bucket_size - 1))
                {
                    if (i != 0)
                    {
                        List<string> tmp_list = new List<string>();
                        tmp_list.Add(bucket_data[i][0]);
                        // Open , High, Low, Close
                        tmp_list.Add(bucket_open.ToString());
                        tmp_list.Add(bucket_high.ToString());
                        tmp_list.Add(bucket_low.ToString());
                        tmp_list.Add(bucket_close.ToString());

                        tmp_list.Add(buy_volume.ToString());
                        tmp_list.Add(sell_volume.ToString());

                        result.Add(tmp_list);
                        //tmp_list.Clear();
                    }
                }

                if ((i % bucket_size) == (bucket_size - 2))
                    bucket_close = price;

                if (price > bucket_high || bucket_high == 0)
                    bucket_high = price;

                if (price < bucket_low || bucket_low == 0)
                    bucket_low = price;

                if (volume_type == "-")
                    sell_volume++;
                else if (volume_type == "+")
                    buy_volume++;
            }

            return result;
        }

        public int updateTickData(string time, string price, string volume)
        {
            int result = 0;
            List<string> tmp_list = new List<String>();

            // 체결수량이 Bucket Size를 초과할 수 있어서 이렇게 작성함.
            for (int i = 0; i < Math.Abs(int.Parse(volume)); i++)
            {
                this.tick_count++;
                if (this.bucket_size == 0)
                    this.bucket_size = 500;
                if (tick_count % this.bucket_size == 0)
                    result = 1;

                tmp_list.Add(time);
                tmp_list.Add(price);

                if (volume.IndexOf('-') > -1)
                    tmp_list.Add("-");
                else
                    tmp_list.Add("+");
                this.bucket_data.Add(tmp_list);
            }

            return result;
        }
    }
}
