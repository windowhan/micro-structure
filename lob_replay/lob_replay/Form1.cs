﻿using System;
using System.IO;
using System.Diagnostics;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Json;
using System.Text.RegularExpressions;
using System.Globalization;

namespace lob_replay
{
    public partial class Form1 : Form
    {
        private double replay_speed;

        private bool check_replay_speed;
        private bool check_hoga_data;
        private bool check_contract_data;

        private List<List<string>> hoga_data;
        private List<List<string>> contract_data;

        private List<DateTime> hoga_time_data;
        private List<DateTime> contract_time_data;

        private JsonTextParser jsonParser;
        private DateTime hoga_start_time;
        private DateTime contract_start_time;

        private int hoga_data_idx;
        private int contract_data_idx;

        private bool temp_stop_flag;
        private Stopwatch time_watch;

        private long custom_hoga_timeElapsed;
        private long custom_contract_timeElapsed;

        private int chart_type = 0;
        public Form1()
        {
            InitializeComponent();

            jsonParser = new JsonTextParser();

            replay_pause_btn.Enabled = false;
            replay_reset_btn.Enabled = true;
            replay_start_btn.Enabled = true;

            hoga_data = new List<List<string>>();
            contract_data = new List<List<string>>();

            time_watch = new Stopwatch();

            hoga_time_data = new List<DateTime>();
            contract_time_data = new List<DateTime>();

            hoga_data_idx = 0;
            contract_data_idx = 0;

            custom_hoga_timeElapsed = 0;
            custom_contract_timeElapsed = 0;

            temp_stop_flag = false;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            lob_listview.GridLines = true;
            lob_listview.FullRowSelect = true;
            time_trackBar.Value = 0;
        }

        public string ShowFileOpenDialog()
        {
            //파일오픈창 생성 및 설정
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Title = "로그파일 가져오기";
            ofd.FileName = "";
            ofd.Filter = "모든 파일 (*.*) | *.*";
 
            //파일 오픈창 로드
            DialogResult dr = ofd.ShowDialog();
             
            //OK버튼 클릭시
            if (dr == DialogResult.OK)
            {
                string fileFullName = ofd.FileName;

                return fileFullName;
            }
            //취소버튼 클릭시 또는 ESC키로 파일창을 종료 했을경우
            else if (dr == DialogResult.Cancel)
            {
                return "empty";
            }
 
            return "empty";
        }

        private void hogafile_find_btn_Click(object sender, EventArgs e)
        {
            string filename = ShowFileOpenDialog();
            
            if(filename == "empty")
            {
                debug_text.Text = "[" + DateTime.Now.ToString("HH:mm:ss.ffffff") + "] 호가데이터 로그파일을 선택하지 않았습니다.\r\n" + debug_text.Text;
                return;
            }
            hogafile_path_txt.Text = filename;

            // File Read
            try
            {
                this.hoga_data.Clear();
                this.hoga_time_data.Clear();
                using(StreamReader sr = new StreamReader(hogafile_path_txt.Text))
                {
                    string[] all_string = Regex.Split(sr.ReadToEnd(), "\r\n");

                    for (int i = 0; i < all_string.Length-1; i++)
                    {
                        //debug_text.Text = "[" + DateTime.Now.ToString("HH:mm:ss.ffffff") + "] " + all_string[i] + "\r\n" + debug_text.Text;
                        JsonObject obj = jsonParser.Parse(all_string[i].Replace("\"-", "\""));
                        List<string> tmp_hoga = new List<string>();

                        bool time_flag = false;
                        foreach (JsonObject field in obj as JsonObjectCollection)
                        {
                            if(time_flag == false)
                            {
                                if (i == 0)
                                    this.hoga_start_time = DateTime.ParseExact(field.GetValue().ToString(), "HH:mm:ss.ffffff", CultureInfo.InvariantCulture);

                                hoga_time_data.Add(DateTime.ParseExact(field.GetValue().ToString(), "HH:mm:ss.ffffff", CultureInfo.InvariantCulture));
                                time_flag = true;
                            }
                            tmp_hoga.Add(field.GetValue().ToString());
                        }
                        hoga_data.Add(tmp_hoga);
                    }

                    debug_text.Text = "[" + DateTime.Now.ToString("HH:mm:ss.ffffff") + "] 호가데이터 로그파일 로드 완료.\r\n" + debug_text.Text;
                }

                
                this.check_hoga_data = true;
            }
            catch(Exception ex)
            {
                debug_text.Text = "[" + DateTime.Now.ToString("HH:mm:ss.ffffff") + "] Line 78 : " + ex.Message + "\r\n" +  debug_text.Text;
                this.check_hoga_data = false;
            }
        }

        private void contractfile_find_btn_Click(object sender, EventArgs e)
        {
            string filename = ShowFileOpenDialog();

            if (filename == "empty")
            {
                debug_text.Text = "[" + DateTime.Now.ToString("HH:mm:ss.ffffff") + "] 체결데이터 로그파일을 선택하지 않았습니다.\r\n" + debug_text.Text;
                return;
            }
            contractfile_path_txt.Text = filename;

            // File Read
            try
            {
                this.contract_data.Clear();
                this.contract_time_data.Clear();

                using (StreamReader sr = new StreamReader(contractfile_path_txt.Text))
                {
                    string[] all_string = Regex.Split(sr.ReadToEnd(), "\r\n");

                    for (int i = 0; i < all_string.Length-1; i++)
                    {
                        //debug_text.Text = "[" + DateTime.Now.ToString("HH:mm:ss.ffffff") + "] " + all_string[i] + "\r\n" + debug_text.Text;
                        JsonObject obj = jsonParser.Parse(all_string[i].Replace("현재가\":\"-", "현재가\":\""));
                        List<string> tmp_contract = new List<string>();

                        bool time_flag = false;
                        foreach (JsonObject field in obj as JsonObjectCollection)
                        {
                            if (time_flag == false)
                            {
                                if (i == 0)
                                    this.contract_start_time = DateTime.ParseExact(field.GetValue().ToString(), "HH:mm:ss.ffffff", CultureInfo.InvariantCulture);

                                contract_time_data.Add(DateTime.ParseExact(field.GetValue().ToString(), "HH:mm:ss.ffffff", CultureInfo.InvariantCulture));
                                time_flag = true;
                            }
                            tmp_contract.Add(field.GetValue().ToString());
                        }
                        contract_data.Add(tmp_contract);
                    }

                    debug_text.Text = "[" + DateTime.Now.ToString("HH:mm:ss.ffffff") + "] 체결데이터 로그파일 로드 완료.\r\n" + debug_text.Text;
                }

                this.check_contract_data = true;
            }
            catch (Exception ex)
            {
                debug_text.Text = "[" + DateTime.Now.ToString("HH:mm:ss.ffffff") + "] Line 103 : " + ex.Message + "\r\n" + debug_text.Text;
                this.check_contract_data = false;
            }
        }

        private void speed_set_btn_Click(object sender, EventArgs e)
        {
            try
            {
                this.replay_speed = double.Parse(speed_set_txt.Text);
                this.check_replay_speed = true;
                debug_text.Text = "[" + DateTime.Now.ToString("HH:mm:ss.ffffff") + "] " + this.replay_speed.ToString() + "배속 설정 완료.\r\n" + debug_text.Text;
            }
            catch(Exception ex)
            {
                debug_text.Text = "[" + DateTime.Now.ToString("HH:mm:ss.ffffff") + "] Line 117 : " + ex.Message + "\r\n" + debug_text.Text;
                this.check_replay_speed = false;
            }
        }

        private void replay_start_btn_Click(object sender, EventArgs e)
        {
            if(this.check_replay_speed == true && this.check_hoga_data == true && this.check_contract_data == true)
            {
                replay_reset_btn.Enabled = true;
                replay_pause_btn.Enabled = true;
                replay_start_btn.Enabled = false;

                if (temp_stop_flag == false)
                {
                    time_watch.Start();
                    time_trackBar.Maximum = hoga_data.Count;
                    time_trackBar.Value = 0;
                }
                else
                {
                    time_watch.Restart();
                    temp_stop_flag = false;
                }
                hoga_timer.Start();
                contract_timer.Start();

                debug_text.Text = "[" + DateTime.Now.ToString("HH:mm:ss.ffffff") + "] 리플레이를 시작했습니다. \r\n" + debug_text.Text;
            }

        }

        private void time_set_btn_Click(object sender, EventArgs e)
        {

        }

        private void lob_listview_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void hoga_timer_Tick(object sender, EventArgs e)
        {
            long timeElapsed = time_watch.ElapsedTicks + custom_hoga_timeElapsed;
            long data_timeElapsed = (DateTime.ParseExact(hoga_data[hoga_data_idx][0], "HH:mm:ss.ffffff", CultureInfo.InvariantCulture) - hoga_start_time).Ticks;

            if(hoga_data_idx == hoga_data.Count)
            {
                time_watch.Reset();
                replay_reset_btn.Enabled = true;
                replay_pause_btn.Enabled = true;
                replay_start_btn.Enabled = false;

            }

            if (data_timeElapsed < (timeElapsed * this.replay_speed))
            {
                // listview 초기화 
                if (lob_listview.Items.Count != 0)
                {
                    lob_listview.Items.Clear();
                }

                time_trackBar.Value += 1;

                current_time_txt.Text = hoga_data[hoga_data_idx][0];

                // 매도 5호가
                lob_listview.Items.Add(new ListViewItem(new String[] { hoga_data[hoga_data_idx][3], hoga_data[hoga_data_idx][1], hoga_data[hoga_data_idx][2], "", "" }));
                // 매도 4호가
                lob_listview.Items.Add(new ListViewItem(new String[] { hoga_data[hoga_data_idx][6], hoga_data[hoga_data_idx][4], hoga_data[hoga_data_idx][5], "", "" }));
                // 매도 3호가
                lob_listview.Items.Add(new ListViewItem(new String[] { hoga_data[hoga_data_idx][9], hoga_data[hoga_data_idx][7], hoga_data[hoga_data_idx][8], "", "" }));
                // 매도 2호가 
                lob_listview.Items.Add(new ListViewItem(new String[] { hoga_data[hoga_data_idx][12], hoga_data[hoga_data_idx][10], hoga_data[hoga_data_idx][11], "", "" }));
                // 매도 1호가
                lob_listview.Items.Add(new ListViewItem(new String[] { hoga_data[hoga_data_idx][15], hoga_data[hoga_data_idx][13], hoga_data[hoga_data_idx][14], "", "" }));

                // 매수 1호가
                lob_listview.Items.Add(new ListViewItem(new String[] { "", "", hoga_data[hoga_data_idx][29], hoga_data[hoga_data_idx][28], hoga_data[hoga_data_idx][30] }));
                
                // 매수 2호가
                lob_listview.Items.Add(new ListViewItem(new String[] { "", "", hoga_data[hoga_data_idx][26], hoga_data[hoga_data_idx][25], hoga_data[hoga_data_idx][27] }));
                
                // 매수 3호가
                lob_listview.Items.Add(new ListViewItem(new String[] { "", "", hoga_data[hoga_data_idx][23], hoga_data[hoga_data_idx][22], hoga_data[hoga_data_idx][24] }));
                // 매수 4호가
                lob_listview.Items.Add(new ListViewItem(new String[] { "", "", hoga_data[hoga_data_idx][20], hoga_data[hoga_data_idx][19], hoga_data[hoga_data_idx][21] }));
                // 매수 5호가
                lob_listview.Items.Add(new ListViewItem(new String[] { "", "", hoga_data[hoga_data_idx][17], hoga_data[hoga_data_idx][16], hoga_data[hoga_data_idx][18] }));

                int 매수_건수_총합 = int.Parse(hoga_data[hoga_data_idx][3]) + int.Parse(hoga_data[hoga_data_idx][6]) + int.Parse(hoga_data[hoga_data_idx][9]) + int.Parse(hoga_data[hoga_data_idx][12]) + int.Parse(hoga_data[hoga_data_idx][15]);
                int 매수_물량_총합 = int.Parse(hoga_data[hoga_data_idx][1]) + int.Parse(hoga_data[hoga_data_idx][4]) + int.Parse(hoga_data[hoga_data_idx][7]) + int.Parse(hoga_data[hoga_data_idx][10]) + int.Parse(hoga_data[hoga_data_idx][13]);
                int 매도_건수_총합 = int.Parse(hoga_data[hoga_data_idx][18]) + int.Parse(hoga_data[hoga_data_idx][21]) + int.Parse(hoga_data[hoga_data_idx][24]) + int.Parse(hoga_data[hoga_data_idx][27]) + int.Parse(hoga_data[hoga_data_idx][30]);
                int 매도_물량_총합 = int.Parse(hoga_data[hoga_data_idx][16]) + int.Parse(hoga_data[hoga_data_idx][19]) + int.Parse(hoga_data[hoga_data_idx][22]) + int.Parse(hoga_data[hoga_data_idx][25]) + int.Parse(hoga_data[hoga_data_idx][28]);


                lob_listview.Items.Add(new ListViewItem(new String[] {매도_건수_총합.ToString(), 매도_물량_총합.ToString(), (매수_물량_총합-매도_물량_총합).ToString(), 매수_물량_총합.ToString(), 매수_건수_총합.ToString()}));
                lob_listview.Items.Add(new ListViewItem(new String[] {"", "", hoga_data[hoga_data_idx][0], "", "" }));
                //(new ListViewItem(new String[] { hoga_data[hoga_data_idx][1] }));
                //debug_text.Text = "timeElapsed : " + timeElapsed.ToString() + " | data_timeElapsed : " + data_timeElapsed.ToString() + "\r\n" + debug_text.Text;
                //debug_text.Text = "데이터 시간 :  " + hoga_data[hoga_data_idx][0] + " | Now Time : " + DateTime.Now.ToString("HH:mm:ss.ffffff") + "\r\n" + debug_text.Text;
                hoga_data_idx++;
            }

            //debug_text.Text = timeElapsed.TotalSeconds.ToString("0.000");
        }

        private void contract_timer_Tick(object sender, EventArgs e)
        {
            long timeElapsed = time_watch.ElapsedTicks + custom_contract_timeElapsed;
            long data_timeElapsed = (DateTime.ParseExact(contract_data[contract_data_idx][0], "HH:mm:ss.ffffff", CultureInfo.InvariantCulture) - contract_start_time).Ticks;
            if (contract_time_data[contract_data_idx] < hoga_time_data[hoga_data_idx])
            {
                contract_list.Items.Add("[" + contract_data[contract_data_idx][0] + "] " + contract_data[contract_data_idx][2] + "   " + contract_data[contract_data_idx][4]);
                contract_data_idx++;
                contract_list.SelectedIndex = contract_list.Items.Count - 1;
            }
        }

        private void replay_pause_btn_Click(object sender, EventArgs e)
        {
            hoga_timer.Stop();
            contract_timer.Stop();

            temp_stop_flag = true;

            time_watch.Stop();
            replay_start_btn.Enabled = true;
            replay_pause_btn.Enabled = false;
            debug_text.Text = "[" + DateTime.Now.ToString("HH:mm:ss.ffffff") + "] 리플레이를 중지했습니다. \r\n" + debug_text.Text;
        }

        private void replay_reset_btn_Click(object sender, EventArgs e)
        {
            lob_listview.Items.Clear();
            contract_list.Items.Clear();

            hoga_timer.Stop();
            contract_timer.Stop();

            time_watch.Stop();
            time_watch.Reset();

            time_trackBar.Maximum = hoga_data.Count;
            time_trackBar.Value = 0;
            debug_text.Text = "[" + DateTime.Now.ToString("HH:mm:ss.ffffff") + "] 리플레이 상태를 초기화했습니다. \r\n" + debug_text.Text;
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void time_trackBar_Scroll(object sender, EventArgs e)
        {
            current_time_txt.Text = hoga_time_data[time_trackBar.Value].ToString();
            hoga_data_idx = time_trackBar.Value;

            for(int i=0;i<contract_time_data.Count;i++)
            {
                if(hoga_time_data[time_trackBar.Value] < contract_time_data[i])
                {
                    contract_list.Items.Clear();
                    contract_data_idx = i - 1;
                    break;
                }
            }

            debug_text.Text = "[" + DateTime.Now.ToString("HH:mm:ss.ffffff") + "] 체결창 time : " + contract_time_data[contract_data_idx].ToString() + " \r\n" + debug_text.Text;
            debug_text.Text = "[" + DateTime.Now.ToString("HH:mm:ss.ffffff") + "] 호가창 time : " + hoga_time_data[hoga_data_idx].ToString() + " \r\n" + debug_text.Text;

            custom_hoga_timeElapsed = (DateTime.ParseExact(hoga_data[hoga_data_idx][0], "HH:mm:ss.ffffff", CultureInfo.InvariantCulture) - hoga_start_time).Ticks;
            custom_contract_timeElapsed = (DateTime.ParseExact(contract_data[contract_data_idx][0], "HH:mm:ss.ffffff", CultureInfo.InvariantCulture) - contract_start_time).Ticks;

            debug_text.Text = "[" + DateTime.Now.ToString("HH:mm:ss.ffffff") + "] custom_hoga_timeElapsed : " + custom_hoga_timeElapsed.ToString() + " \r\n" + debug_text.Text;
            debug_text.Text = "[" + DateTime.Now.ToString("HH:mm:ss.ffffff") + "] custom_contract_timeElapsed : " + custom_contract_timeElapsed.ToString() + " \r\n" + debug_text.Text;
            
            //contract_data_idx = time_trackBar.Value;
        }
    }
}
