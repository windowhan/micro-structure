﻿namespace lob_replay
{
    partial class VPIN_form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend3 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea4 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend4 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.bucket_count_label = new System.Windows.Forms.Label();
            this.set_sample_length_btn = new System.Windows.Forms.Button();
            this.sample_length_txt = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.setbs_btn = new System.Windows.Forms.Button();
            this.bucketsize_txt = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.chart_y_down_btn = new System.Windows.Forms.Button();
            this.chart_y_up_btn = new System.Windows.Forms.Button();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.debug_text = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.bucket_listview = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.chart2 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.bucket_count_label);
            this.groupBox1.Controls.Add(this.set_sample_length_btn);
            this.groupBox1.Controls.Add(this.sample_length_txt);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.setbs_btn);
            this.groupBox1.Controls.Add(this.bucketsize_txt);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1048, 52);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "설정";
            // 
            // bucket_count_label
            // 
            this.bucket_count_label.AutoSize = true;
            this.bucket_count_label.Location = new System.Drawing.Point(691, 25);
            this.bucket_count_label.Name = "bucket_count_label";
            this.bucket_count_label.Size = new System.Drawing.Size(89, 12);
            this.bucket_count_label.TabIndex = 6;
            this.bucket_count_label.Text = "bucket count : ";
            // 
            // set_sample_length_btn
            // 
            this.set_sample_length_btn.Location = new System.Drawing.Point(595, 20);
            this.set_sample_length_btn.Name = "set_sample_length_btn";
            this.set_sample_length_btn.Size = new System.Drawing.Size(90, 23);
            this.set_sample_length_btn.TabIndex = 5;
            this.set_sample_length_btn.Text = "설정";
            this.set_sample_length_btn.UseVisualStyleBackColor = true;
            this.set_sample_length_btn.Click += new System.EventHandler(this.set_sample_length_btn_Click);
            // 
            // sample_length_txt
            // 
            this.sample_length_txt.Location = new System.Drawing.Point(437, 20);
            this.sample_length_txt.Name = "sample_length_txt";
            this.sample_length_txt.Size = new System.Drawing.Size(152, 21);
            this.sample_length_txt.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(346, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 12);
            this.label2.TabIndex = 3;
            this.label2.Text = "sample length";
            // 
            // setbs_btn
            // 
            this.setbs_btn.Location = new System.Drawing.Point(247, 20);
            this.setbs_btn.Name = "setbs_btn";
            this.setbs_btn.Size = new System.Drawing.Size(90, 23);
            this.setbs_btn.TabIndex = 2;
            this.setbs_btn.Text = "설정";
            this.setbs_btn.UseVisualStyleBackColor = true;
            this.setbs_btn.Click += new System.EventHandler(this.setbs_btn_Click);
            // 
            // bucketsize_txt
            // 
            this.bucketsize_txt.Location = new System.Drawing.Point(89, 20);
            this.bucketsize_txt.Name = "bucketsize_txt";
            this.bucketsize_txt.Size = new System.Drawing.Size(152, 21);
            this.bucketsize_txt.TabIndex = 1;
            this.bucketsize_txt.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "bucket size";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.chart_y_down_btn);
            this.groupBox2.Controls.Add(this.chart_y_up_btn);
            this.groupBox2.Controls.Add(this.chart1);
            this.groupBox2.Location = new System.Drawing.Point(738, 70);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(634, 489);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "변화 추이";
            // 
            // chart_y_down_btn
            // 
            this.chart_y_down_btn.Location = new System.Drawing.Point(96, 14);
            this.chart_y_down_btn.Name = "chart_y_down_btn";
            this.chart_y_down_btn.Size = new System.Drawing.Size(90, 23);
            this.chart_y_down_btn.TabIndex = 8;
            this.chart_y_down_btn.Text = "축소";
            this.chart_y_down_btn.UseVisualStyleBackColor = true;
            this.chart_y_down_btn.Click += new System.EventHandler(this.chart_y_down_btn_Click);
            // 
            // chart_y_up_btn
            // 
            this.chart_y_up_btn.Location = new System.Drawing.Point(6, 14);
            this.chart_y_up_btn.Name = "chart_y_up_btn";
            this.chart_y_up_btn.Size = new System.Drawing.Size(90, 23);
            this.chart_y_up_btn.TabIndex = 7;
            this.chart_y_up_btn.Text = "확대";
            this.chart_y_up_btn.UseVisualStyleBackColor = true;
            this.chart_y_up_btn.Click += new System.EventHandler(this.chart_y_up_btn_Click);
            // 
            // chart1
            // 
            chartArea3.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea3);
            legend3.Name = "Legend1";
            this.chart1.Legends.Add(legend3);
            this.chart1.Location = new System.Drawing.Point(6, 43);
            this.chart1.Name = "chart1";
            series3.ChartArea = "ChartArea1";
            series3.Legend = "Legend1";
            series3.Name = "Series1";
            this.chart1.Series.Add(series3);
            this.chart1.Size = new System.Drawing.Size(622, 440);
            this.chart1.TabIndex = 0;
            this.chart1.Text = "chart1";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.debug_text);
            this.groupBox3.Location = new System.Drawing.Point(12, 408);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(720, 151);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "디버깅 로그";
            // 
            // debug_text
            // 
            this.debug_text.Location = new System.Drawing.Point(6, 20);
            this.debug_text.Multiline = true;
            this.debug_text.Name = "debug_text";
            this.debug_text.Size = new System.Drawing.Size(705, 125);
            this.debug_text.TabIndex = 0;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.bucket_listview);
            this.groupBox4.Location = new System.Drawing.Point(12, 70);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(720, 332);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Bucket 리스트";
            // 
            // bucket_listview
            // 
            this.bucket_listview.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader6,
            this.columnHeader7,
            this.columnHeader8,
            this.columnHeader9,
            this.columnHeader10});
            this.bucket_listview.Location = new System.Drawing.Point(0, 20);
            this.bucket_listview.Name = "bucket_listview";
            this.bucket_listview.Size = new System.Drawing.Size(705, 306);
            this.bucket_listview.TabIndex = 0;
            this.bucket_listview.UseCompatibleStateImageBehavior = false;
            this.bucket_listview.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "시간";
            this.columnHeader1.Width = 99;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "시가";
            this.columnHeader2.Width = 52;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "고가";
            this.columnHeader3.Width = 55;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "저가";
            this.columnHeader4.Width = 51;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "종가";
            this.columnHeader5.Width = 48;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "bucket size";
            this.columnHeader6.Width = 84;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "매수비율";
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "매도비율";
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "매수매도차";
            this.columnHeader9.Width = 85;
            // 
            // columnHeader10
            // 
            this.columnHeader10.Text = "VPIN";
            this.columnHeader10.Width = 61;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.chart2);
            this.groupBox5.Location = new System.Drawing.Point(12, 565);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(720, 334);
            this.groupBox5.TabIndex = 4;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Bucket Chart";
            // 
            // chart2
            // 
            chartArea4.Name = "ChartArea1";
            this.chart2.ChartAreas.Add(chartArea4);
            legend4.Name = "Legend1";
            this.chart2.Legends.Add(legend4);
            this.chart2.Location = new System.Drawing.Point(6, 20);
            this.chart2.Name = "chart2";
            series4.ChartArea = "ChartArea1";
            series4.Legend = "Legend1";
            series4.Name = "Series1";
            this.chart2.Series.Add(series4);
            this.chart2.Size = new System.Drawing.Size(705, 308);
            this.chart2.TabIndex = 9;
            this.chart2.Text = "chart2";
            // 
            // VPIN_form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1384, 903);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "VPIN_form";
            this.Text = "VPIN 측정";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.calculate_var_form_FormClosing);
            this.Load += new System.EventHandler(this.VPIN_form_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox debug_text;
        private System.Windows.Forms.Button setbs_btn;
        private System.Windows.Forms.TextBox bucketsize_txt;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.ListView bucket_listview;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ColumnHeader columnHeader10;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.Button set_sample_length_btn;
        private System.Windows.Forms.TextBox sample_length_txt;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label bucket_count_label;
        private System.Windows.Forms.Button chart_y_down_btn;
        private System.Windows.Forms.Button chart_y_up_btn;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart2;
    }
}