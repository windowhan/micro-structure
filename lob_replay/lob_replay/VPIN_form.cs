﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lob_replay
{
    public partial class VPIN_form : Form
    {
        private VPIN_model model_obj;
        private int bucket_size;
        private int sample_length;

        public VPIN_form()
        {
            InitializeComponent();
            this.model_obj = new VPIN_model();
            this.bucket_size = 0;
            this.sample_length = 0;
        }

        private void calculate_var_form_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            this.Hide();
        }

        private void var_list_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        public void recvRealtimeData(List<string> data)
        {
            //debug_text.Text = "[" + DateTime.Now.ToString("HH:mm:ss.ffffff") + "] 실시간 수신 데이터 : " + data.ToString() + "\r\n" + debug_text.Text;
            // 0 - 시간, 2 - 가격, 4 - 체결량
            this.setbs_btn.Enabled = false;
            if(this.model_obj.updateTickData(data[0], data[2], data[4]) == 1)
            {
                //bucket_listview.Items.Clear();
                List<List<string>> tmp_list = this.model_obj.getBucketInfo();
                if(tmp_list.Count>bucket_listview.Items.Count)
                {
                    for(int i=tmp_list.Count-(tmp_list.Count-bucket_listview.Items.Count);i<tmp_list.Count;i++)
                    {
                        double vpin = 0;
                        if (bucket_listview.Items.Count > this.sample_length)
                        {
                            int oi_value = 0;
                            for (int j = (bucket_listview.Items.Count - 1); j > (bucket_listview.Items.Count - this.sample_length - 1); j--)
                            {
                                oi_value += int.Parse(bucket_listview.Items[j].SubItems[8].Text);
                            }
                            vpin = (double)oi_value / (this.bucket_size * this.sample_length);
                        }
                        
                            //double vpin = Math.Truncate(value_sum == 0 ? 0:((double)value_sum / (this.bucket_size*bucket_listview.Items.Count))*1000)/1000;
                        bucket_listview.Items.Add(new ListViewItem(new String[] { tmp_list[i][0], tmp_list[i][1], tmp_list[i][2], tmp_list[i][3], tmp_list[i][4], this.bucketsize_txt.Text, tmp_list[i][5], tmp_list[i][6], Math.Abs(int.Parse(tmp_list[i][5]) - int.Parse(tmp_list[i][6])).ToString(),vpin.ToString() }));
                        
                        this.chart1.ChartAreas["CHART"].AxisX.Minimum = 0;
                        this.chart1.ChartAreas["CHART"].AxisX.Maximum = bucket_listview.Items.Count + 2;

                        if (bucket_listview.Items.Count > this.sample_length)
                        {
                            this.chart1.Series["VPIN"].Points.AddXY(chart1.Series["VPIN"].Points.Count, vpin);
                        }
                        this.bucket_count_label.Text = "bucket count : " + bucket_listview.Items.Count.ToString();


                        //chart2.Series[0].Points.AddXY(tmp_list[i][0], tmp_list[i][2], tmp_list[i][3], tmp_list[i][1], tmp_list[i][4] });
                        chart2.Series[0].Points.AddXY(tmp_list[i][0], double.Parse(tmp_list[i][2]));
                        chart2.Series[0].Points[chart2.Series[0].Points.Count - 1].YValues[1] = double.Parse(tmp_list[i][3]);
                        chart2.Series[0].Points[chart2.Series[0].Points.Count - 1].YValues[2] = double.Parse(tmp_list[i][1]);
                        chart2.Series[0].Points[chart2.Series[0].Points.Count - 1].YValues[3] = double.Parse(tmp_list[i][4]);
                    }
                }
            }
        }

        private void setbs_btn_Click(object sender, EventArgs e)
        {
            model_obj.setBucketSize(int.Parse(bucketsize_txt.Text));
            this.bucket_size = int.Parse(bucketsize_txt.Text);
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void VPIN_form_Load(object sender, EventArgs e)
        {
            bucket_listview.GridLines = true;
            bucket_listview.FullRowSelect = true;

            this.bucketsize_txt.Text = "500";
            this.bucket_size = 500;
            this.model_obj.setBucketSize(500);

            this.chart1.ChartAreas.Clear();
            this.chart1.Series.Clear();

            // Chart Area 추가 
            this.chart1.ChartAreas.Add("CHART");
            this.chart1.ChartAreas["CHART"].BackColor = Color.Black;

            // X축 설정
            this.chart1.ChartAreas["CHART"].AxisY.MajorGrid.LineColor = Color.Gray;
            this.chart1.ChartAreas["CHART"].AxisY.MajorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dash;

            // Y축 설정
            this.chart1.ChartAreas["CHART"].AxisY.Minimum = 0;
            this.chart1.ChartAreas["CHART"].AxisY.Maximum = 1;
            this.chart1.ChartAreas["CHART"].AxisY.MajorGrid.LineColor = Color.Gray;
            this.chart1.ChartAreas["CHART"].AxisY.MajorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dash;

            this.chart1.Series.Add("VPIN");
            this.chart1.Series["VPIN"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            this.chart1.Series["VPIN"].Color = Color.LightGreen;
            this.chart1.Series["VPIN"].BorderWidth = 2;
            this.chart1.Series["VPIN"].LegendText = "VPIN";


            // bucket chart 설정
            //chart2.ChartAreas["ChartArea1"].AxisX.MajorGrid.LineWidth = 0;
            //chart2.ChartAreas["ChartArea1"].AxisY.MajorGrid.LineWidth = 0;

            chart2.Series[0].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Candlestick;

            chart2.Series[0].XValueMember = "Time";
            chart2.Series[0].YValueMembers = "High,Low,Open,Close";
            chart2.Series[0].XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Time;
            chart2.Series[0].CustomProperties = "PriceDownColor=Red,PriceUpColor=Blue";
            //chart1.Series[0]["OpenCloseStyle"] = "Triangle";
            chart2.Series[0]["ShowOpenClose"] = "Both";

            chart2.ChartAreas[0].AxisX.ScaleView.MinSizeType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Minutes;
            chart2.ChartAreas[0].AxisX.ScaleView.SmallScrollMinSizeType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Minutes;
            chart2.ChartAreas[0].AxisY.IsStartedFromZero = false;
            chart2.ChartAreas[0].CursorX.IntervalOffsetType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Minutes;
            chart2.ChartAreas[0].CursorX.IsUserSelectionEnabled = true;
            chart2.ChartAreas[0].Name = "ChartArea1";
            chart2.DataManipulator.IsStartFromFirst = true;

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
        }

        private void set_sample_length_btn_Click(object sender, EventArgs e)
        {
            this.sample_length = int.Parse(this.sample_length_txt.Text);
        }

        private void chart_y_up_btn_Click(object sender, EventArgs e)
        {
            this.chart1.ChartAreas["CHART"].AxisY.Maximum -= 0.05;
        }

        private void chart_y_down_btn_Click(object sender, EventArgs e)
        {
            this.chart1.ChartAreas["CHART"].AxisY.Maximum += 0.05;
        }
    }
}
