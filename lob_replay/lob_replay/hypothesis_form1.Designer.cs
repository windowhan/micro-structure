﻿namespace lob_replay
{
    partial class hypothesis_form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title1 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title2 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend3 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title3 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea4 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend4 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series5 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title4 = new System.Windows.Forms.DataVisualization.Charting.Title();
            this.total_chart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.asia_chart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.europe_chart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.usa_chart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.log_box = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.total_chart)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.asia_chart)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.europe_chart)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.usa_chart)).BeginInit();
            this.SuspendLayout();
            // 
            // total_chart
            // 
            chartArea1.Name = "ChartArea1";
            this.total_chart.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.total_chart.Legends.Add(legend1);
            this.total_chart.Location = new System.Drawing.Point(16, 20);
            this.total_chart.Name = "total_chart";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "BUY";
            series1.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.String;
            series1.YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Int32;
            series2.ChartArea = "ChartArea1";
            series2.Legend = "Legend1";
            series2.Name = "SELL";
            series2.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.String;
            series2.YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Int32;
            this.total_chart.Series.Add(series1);
            this.total_chart.Series.Add(series2);
            this.total_chart.Size = new System.Drawing.Size(507, 410);
            this.total_chart.TabIndex = 0;
            title1.Name = "Title1";
            title1.Text = "종합 매수 통계";
            this.total_chart.Titles.Add(title1);
            this.total_chart.Click += new System.EventHandler(this.chart1_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.total_chart);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(536, 443);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "종합";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.asia_chart);
            this.groupBox2.Location = new System.Drawing.Point(554, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(628, 443);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "아시아장";
            // 
            // asia_chart
            // 
            chartArea2.Name = "ChartArea1";
            this.asia_chart.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            this.asia_chart.Legends.Add(legend2);
            this.asia_chart.Location = new System.Drawing.Point(16, 20);
            this.asia_chart.Name = "asia_chart";
            series3.ChartArea = "ChartArea1";
            series3.Legend = "Legend1";
            series3.Name = "Series1";
            this.asia_chart.Series.Add(series3);
            this.asia_chart.Size = new System.Drawing.Size(601, 410);
            this.asia_chart.TabIndex = 0;
            this.asia_chart.Text = "dddd";
            title2.Name = "Title1";
            title2.Text = "아시아장 통계";
            this.asia_chart.Titles.Add(title2);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.europe_chart);
            this.groupBox3.Location = new System.Drawing.Point(12, 461);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(536, 443);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "유럽장";
            // 
            // europe_chart
            // 
            chartArea3.Name = "ChartArea1";
            this.europe_chart.ChartAreas.Add(chartArea3);
            legend3.Name = "Legend1";
            this.europe_chart.Legends.Add(legend3);
            this.europe_chart.Location = new System.Drawing.Point(16, 20);
            this.europe_chart.Name = "europe_chart";
            series4.ChartArea = "ChartArea1";
            series4.Legend = "Legend1";
            series4.Name = "Series1";
            this.europe_chart.Series.Add(series4);
            this.europe_chart.Size = new System.Drawing.Size(507, 410);
            this.europe_chart.TabIndex = 0;
            this.europe_chart.Text = "dddd";
            title3.Name = "Title1";
            title3.Text = "유럽장 통계";
            this.europe_chart.Titles.Add(title3);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.usa_chart);
            this.groupBox4.Location = new System.Drawing.Point(554, 461);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(628, 443);
            this.groupBox4.TabIndex = 4;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "미국장";
            // 
            // usa_chart
            // 
            chartArea4.Name = "ChartArea1";
            this.usa_chart.ChartAreas.Add(chartArea4);
            legend4.Name = "Legend1";
            this.usa_chart.Legends.Add(legend4);
            this.usa_chart.Location = new System.Drawing.Point(16, 20);
            this.usa_chart.Name = "usa_chart";
            series5.ChartArea = "ChartArea1";
            series5.Legend = "Legend1";
            series5.Name = "Series1";
            this.usa_chart.Series.Add(series5);
            this.usa_chart.Size = new System.Drawing.Size(601, 410);
            this.usa_chart.TabIndex = 0;
            this.usa_chart.Text = "dddd";
            title4.Name = "Title1";
            title4.Text = "미국장 통계";
            this.usa_chart.Titles.Add(title4);
            // 
            // log_box
            // 
            this.log_box.Location = new System.Drawing.Point(12, 910);
            this.log_box.Multiline = true;
            this.log_box.Name = "log_box";
            this.log_box.Size = new System.Drawing.Size(1170, 163);
            this.log_box.TabIndex = 5;
            // 
            // hypothesis_form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1191, 1085);
            this.Controls.Add(this.log_box);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.HelpButton = true;
            this.Name = "hypothesis_form1";
            this.Text = "개인/기관 물량 분석 가설";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.hypothesis_form1_FormClosing);
            this.Load += new System.EventHandler(this.hypothesis_form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.total_chart)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.asia_chart)).EndInit();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.europe_chart)).EndInit();
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.usa_chart)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart total_chart;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataVisualization.Charting.Chart asia_chart;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DataVisualization.Charting.Chart europe_chart;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.DataVisualization.Charting.Chart usa_chart;
        private System.Windows.Forms.TextBox log_box;
    }
}