﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lob_replay
{
    public partial class hypothesis_form1 : Form
    {
        List<List<string>> tick_data;
        List<string> buy_range_list;
        List<string> sell_range_list;

        public hypothesis_form1()
        {
            InitializeComponent();
            this.tick_data = new List<List<string>>();
            this.buy_range_list = new List<string>();
            this.sell_range_list = new List<string>();
            // 초기 차트 세팅
        }

        private void hypothesis_form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            this.Hide();
        }

        public void recvRealtimeData(List<string> data)
        {
            this.tick_data.Add(data);

            if(this.tick_data.Count<4)
                return;

            List<string> current_data = this.tick_data[this.tick_data.Count - 1];
            List<string> prev_data = this.tick_data[this.tick_data.Count - 2];
            List<string> prev_prev_data = this.tick_data[this.tick_data.Count - 3];

            int end_num = 0;
            string range_name = "";
            end_num = int.Parse(Regex.Split(prev_data[2].Replace("+", "").Replace("-", ""), "\\.")[1]);
            range_name = Regex.Split(prev_data[2].Replace("+", "").Replace("-", ""), "\\.")[0] + "." + (end_num / 10).ToString();// +" - " + Regex.Split(prev_data[2].Replace("+", "").Replace("-", ""), "\\.")[0] + "." + (1 + (end_num / 10)).ToString();


            // Total chart
            if(current_data[4].IndexOf("-")>-1 && prev_data[4].IndexOf("+")>-1 && prev_prev_data[4].IndexOf("-")>-1)
            {
                // BUY CHART\
                if (this.buy_range_list.IndexOf("B" + range_name) < 0)
                {
                    this.total_chart.Series[0].Points.AddXY(range_name, 1);
                    this.buy_range_list.Add("B" + range_name);
                }

                else
                {
                    this.total_chart.Series[0].Points[this.buy_range_list.IndexOf("B" + range_name)].YValues[0] = this.total_chart.Series[0].Points[this.buy_range_list.IndexOf("B" + range_name)].YValues[0] + 1;
                    this.total_chart.Series[0].Points[this.buy_range_list.IndexOf("B" + range_name)].Label = this.total_chart.Series[0].Points[this.buy_range_list.IndexOf("B" + range_name)].YValues[0].ToString();
                }

                this.total_chart.ChartAreas[0].RecalculateAxesScale();
                //log_box.Text = "[" + prev_data[0] + "] 매수 개인추정 물량 확인" + "\r\n" + log_box.Text;
            }

            else if(current_data[4].IndexOf("+")>-1 && prev_data[4].IndexOf("-")>-1 && prev_prev_data[4].IndexOf("+")>-1)
            {
                // SELL CHART   
                if (this.sell_range_list.IndexOf("S" + range_name) < 0)
                {
                    this.total_chart.Series[1].Points.AddXY(range_name, 1);
                    this.sell_range_list.Add("S" + range_name);
                }
                else
                {
                    this.total_chart.Series[1].Points[this.sell_range_list.IndexOf("S" + range_name)].YValues[0] = this.total_chart.Series[1].Points[this.sell_range_list.IndexOf("S" + range_name)].YValues[0] + 1;
                    this.total_chart.Series[1].Points[this.sell_range_list.IndexOf("S" + range_name)].Label = this.total_chart.Series[1].Points[this.sell_range_list.IndexOf("S" + range_name)].YValues[0].ToString();
                }
                this.total_chart.ChartAreas[0].RecalculateAxesScale();
                //log_box.Text = "[" + prev_data[0] + "] 매도 개인추정 물량 확인" + "\r\n" + log_box.Text;
            }

            this.tick_data.RemoveAt(this.tick_data.Count - 3);
            // detail chart
        }

        private void hypothesis_form1_Load(object sender, EventArgs e)
        {

        }

        private void chart1_Click(object sender, EventArgs e)
        {

        }
    }
}
