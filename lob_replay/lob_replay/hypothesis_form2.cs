﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lob_replay
{
    public partial class hypothesis_form2 : Form
    {
        public hypothesis_form2()
        {
            InitializeComponent();
        }

        private void hypothesis_form2_Load(object sender, EventArgs e)
        {
        }

        public void recvRealtimeData(List<string> data)
        {
            int contract_mount = int.Parse(data[4]);
            string range_name = "";
            int end_num = int.Parse(Regex.Split(data[2].Replace("+", "").Replace("-", ""), "\\.")[1]);

            if(contract_mount%5==0)
            {
                range_name = Regex.Split(data[2].Replace("+", "").Replace("-", ""), "\\.")[0] + "." + (end_num / 10).ToString();
            }
        }

        private void hypothesis_form2_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            this.Hide();
        }
    }
}
