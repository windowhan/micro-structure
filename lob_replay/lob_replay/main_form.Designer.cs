﻿namespace lob_replay
{
    partial class main_form
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다.
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lob_listview = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.contract_list = new System.Windows.Forms.ListBox();
            this.speed_set_btn = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.time_trackBar = new System.Windows.Forms.TrackBar();
            this.current_time_txt = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.replay_reset_btn = new System.Windows.Forms.Button();
            this.replay_pause_btn = new System.Windows.Forms.Button();
            this.replay_start_btn = new System.Windows.Forms.Button();
            this.contractfile_find_btn = new System.Windows.Forms.Button();
            this.contractfile_path_txt = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.hogafile_find_btn = new System.Windows.Forms.Button();
            this.hogafile_path_txt = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.speed_set_txt = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.debug_text = new System.Windows.Forms.TextBox();
            this.hoga_timer = new System.Windows.Forms.Timer(this.components);
            this.contract_timer = new System.Windows.Forms.Timer(this.components);
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.chart_view_btn = new System.Windows.Forms.Button();
            this.calc_var_form_btn = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.time_trackBar)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // lob_listview
            // 
            this.lob_listview.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5});
            this.lob_listview.Location = new System.Drawing.Point(12, 12);
            this.lob_listview.Name = "lob_listview";
            this.lob_listview.Size = new System.Drawing.Size(505, 328);
            this.lob_listview.TabIndex = 0;
            this.lob_listview.UseCompatibleStateImageBehavior = false;
            this.lob_listview.View = System.Windows.Forms.View.Details;
            this.lob_listview.SelectedIndexChanged += new System.EventHandler(this.lob_listview_SelectedIndexChanged);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "매도건수";
            this.columnHeader1.Width = 100;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "매도잔량";
            this.columnHeader2.Width = 100;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "호가";
            this.columnHeader3.Width = 100;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "매수잔량";
            this.columnHeader4.Width = 100;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "매수건수";
            this.columnHeader5.Width = 100;
            // 
            // contract_list
            // 
            this.contract_list.FormattingEnabled = true;
            this.contract_list.ItemHeight = 12;
            this.contract_list.Location = new System.Drawing.Point(534, 12);
            this.contract_list.Name = "contract_list";
            this.contract_list.Size = new System.Drawing.Size(196, 328);
            this.contract_list.TabIndex = 1;
            // 
            // speed_set_btn
            // 
            this.speed_set_btn.Location = new System.Drawing.Point(197, 20);
            this.speed_set_btn.Name = "speed_set_btn";
            this.speed_set_btn.Size = new System.Drawing.Size(65, 23);
            this.speed_set_btn.TabIndex = 2;
            this.speed_set_btn.Text = "설정";
            this.speed_set_btn.UseVisualStyleBackColor = true;
            this.speed_set_btn.Click += new System.EventHandler(this.speed_set_btn_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.time_trackBar);
            this.groupBox1.Controls.Add(this.current_time_txt);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.replay_reset_btn);
            this.groupBox1.Controls.Add(this.replay_pause_btn);
            this.groupBox1.Controls.Add(this.replay_start_btn);
            this.groupBox1.Controls.Add(this.contractfile_find_btn);
            this.groupBox1.Controls.Add(this.contractfile_path_txt);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.hogafile_find_btn);
            this.groupBox1.Controls.Add(this.hogafile_path_txt);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.speed_set_txt);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.speed_set_btn);
            this.groupBox1.Location = new System.Drawing.Point(12, 346);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(718, 187);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "리플레이 설정";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(459, 172);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(252, 12);
            this.label5.TabIndex = 18;
            this.label5.Text = "제작자 : 한호정 (window__han@naver.com)";
            // 
            // time_trackBar
            // 
            this.time_trackBar.Location = new System.Drawing.Point(55, 52);
            this.time_trackBar.Name = "time_trackBar";
            this.time_trackBar.Size = new System.Drawing.Size(207, 45);
            this.time_trackBar.TabIndex = 17;
            this.time_trackBar.Scroll += new System.EventHandler(this.time_trackBar_Scroll);
            // 
            // current_time_txt
            // 
            this.current_time_txt.Enabled = false;
            this.current_time_txt.Location = new System.Drawing.Point(55, 103);
            this.current_time_txt.Name = "current_time_txt";
            this.current_time_txt.Size = new System.Drawing.Size(207, 21);
            this.current_time_txt.TabIndex = 15;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(20, 52);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 12);
            this.label4.TabIndex = 14;
            this.label4.Text = "시간";
            // 
            // replay_reset_btn
            // 
            this.replay_reset_btn.Enabled = false;
            this.replay_reset_btn.Location = new System.Drawing.Point(197, 130);
            this.replay_reset_btn.Name = "replay_reset_btn";
            this.replay_reset_btn.Size = new System.Drawing.Size(65, 23);
            this.replay_reset_btn.TabIndex = 13;
            this.replay_reset_btn.Text = "리셋";
            this.replay_reset_btn.UseVisualStyleBackColor = true;
            this.replay_reset_btn.Click += new System.EventHandler(this.replay_reset_btn_Click);
            // 
            // replay_pause_btn
            // 
            this.replay_pause_btn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.replay_pause_btn.Enabled = false;
            this.replay_pause_btn.Location = new System.Drawing.Point(126, 130);
            this.replay_pause_btn.Name = "replay_pause_btn";
            this.replay_pause_btn.Size = new System.Drawing.Size(65, 23);
            this.replay_pause_btn.TabIndex = 12;
            this.replay_pause_btn.Text = "정지";
            this.replay_pause_btn.UseVisualStyleBackColor = true;
            this.replay_pause_btn.Click += new System.EventHandler(this.replay_pause_btn_Click);
            // 
            // replay_start_btn
            // 
            this.replay_start_btn.Location = new System.Drawing.Point(55, 130);
            this.replay_start_btn.Name = "replay_start_btn";
            this.replay_start_btn.Size = new System.Drawing.Size(65, 23);
            this.replay_start_btn.TabIndex = 11;
            this.replay_start_btn.Text = "시작";
            this.replay_start_btn.UseVisualStyleBackColor = true;
            this.replay_start_btn.Click += new System.EventHandler(this.replay_start_btn_Click);
            // 
            // contractfile_find_btn
            // 
            this.contractfile_find_btn.Location = new System.Drawing.Point(635, 47);
            this.contractfile_find_btn.Name = "contractfile_find_btn";
            this.contractfile_find_btn.Size = new System.Drawing.Size(75, 23);
            this.contractfile_find_btn.TabIndex = 10;
            this.contractfile_find_btn.Text = "파일 찾기";
            this.contractfile_find_btn.UseVisualStyleBackColor = true;
            this.contractfile_find_btn.Click += new System.EventHandler(this.contractfile_find_btn_Click);
            // 
            // contractfile_path_txt
            // 
            this.contractfile_path_txt.Location = new System.Drawing.Point(372, 47);
            this.contractfile_path_txt.Name = "contractfile_path_txt";
            this.contractfile_path_txt.Size = new System.Drawing.Size(257, 21);
            this.contractfile_path_txt.TabIndex = 9;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(273, 50);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(93, 12);
            this.label3.TabIndex = 8;
            this.label3.Text = "체결데이터 파일";
            // 
            // hogafile_find_btn
            // 
            this.hogafile_find_btn.Location = new System.Drawing.Point(635, 20);
            this.hogafile_find_btn.Name = "hogafile_find_btn";
            this.hogafile_find_btn.Size = new System.Drawing.Size(75, 23);
            this.hogafile_find_btn.TabIndex = 7;
            this.hogafile_find_btn.Text = "파일 찾기";
            this.hogafile_find_btn.UseVisualStyleBackColor = true;
            this.hogafile_find_btn.Click += new System.EventHandler(this.hogafile_find_btn_Click);
            // 
            // hogafile_path_txt
            // 
            this.hogafile_path_txt.Location = new System.Drawing.Point(372, 20);
            this.hogafile_path_txt.Name = "hogafile_path_txt";
            this.hogafile_path_txt.Size = new System.Drawing.Size(257, 21);
            this.hogafile_path_txt.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(273, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(93, 12);
            this.label2.TabIndex = 5;
            this.label2.Text = "호가데이터 파일";
            // 
            // speed_set_txt
            // 
            this.speed_set_txt.Location = new System.Drawing.Point(55, 20);
            this.speed_set_txt.Name = "speed_set_txt";
            this.speed_set_txt.Size = new System.Drawing.Size(136, 21);
            this.speed_set_txt.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 12);
            this.label1.TabIndex = 3;
            this.label1.Text = "배속";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.debug_text);
            this.groupBox2.Location = new System.Drawing.Point(12, 539);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(718, 117);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "디버깅 로그";
            // 
            // debug_text
            // 
            this.debug_text.Location = new System.Drawing.Point(6, 20);
            this.debug_text.Multiline = true;
            this.debug_text.Name = "debug_text";
            this.debug_text.Size = new System.Drawing.Size(706, 91);
            this.debug_text.TabIndex = 0;
            // 
            // hoga_timer
            // 
            this.hoga_timer.Interval = 1;
            this.hoga_timer.Tick += new System.EventHandler(this.hoga_timer_Tick);
            // 
            // contract_timer
            // 
            this.contract_timer.Interval = 1;
            this.contract_timer.Tick += new System.EventHandler(this.contract_timer_Tick);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.chart_view_btn);
            this.groupBox3.Controls.Add(this.calc_var_form_btn);
            this.groupBox3.Location = new System.Drawing.Point(736, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(101, 644);
            this.groupBox3.TabIndex = 5;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "기타메뉴";
            // 
            // chart_view_btn
            // 
            this.chart_view_btn.Location = new System.Drawing.Point(13, 49);
            this.chart_view_btn.Name = "chart_view_btn";
            this.chart_view_btn.Size = new System.Drawing.Size(75, 23);
            this.chart_view_btn.TabIndex = 19;
            this.chart_view_btn.Text = "가설1";
            this.chart_view_btn.UseVisualStyleBackColor = true;
            this.chart_view_btn.Click += new System.EventHandler(this.chart_view_btn_Click);
            // 
            // calc_var_form_btn
            // 
            this.calc_var_form_btn.Location = new System.Drawing.Point(13, 20);
            this.calc_var_form_btn.Name = "calc_var_form_btn";
            this.calc_var_form_btn.Size = new System.Drawing.Size(75, 23);
            this.calc_var_form_btn.TabIndex = 18;
            this.calc_var_form_btn.Text = "VPIN 측정";
            this.calc_var_form_btn.UseVisualStyleBackColor = true;
            this.calc_var_form_btn.Click += new System.EventHandler(this.button1_Click);
            // 
            // main_form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(848, 668);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.contract_list);
            this.Controls.Add(this.lob_listview);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "main_form";
            this.Text = "호가 리플레이";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.time_trackBar)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView lob_listview;
        private System.Windows.Forms.ListBox contract_list;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.Button speed_set_btn;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button hogafile_find_btn;
        private System.Windows.Forms.TextBox hogafile_path_txt;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox speed_set_txt;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button contractfile_find_btn;
        private System.Windows.Forms.TextBox contractfile_path_txt;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button replay_reset_btn;
        private System.Windows.Forms.Button replay_pause_btn;
        private System.Windows.Forms.Button replay_start_btn;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox debug_text;
        private System.Windows.Forms.TextBox current_time_txt;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Timer hoga_timer;
        private System.Windows.Forms.Timer contract_timer;
        private System.Windows.Forms.TrackBar time_trackBar;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button chart_view_btn;
        private System.Windows.Forms.Button calc_var_form_btn;
        private System.Windows.Forms.Label label5;
    }
}

