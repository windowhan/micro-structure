﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace CL_LOB_snapshot
{
   
    public class Win32Api
    {
        public struct POINTAPI
        {
            public long x;
            public long y;
        }
        /*Public Declare Function WindowFromPoint Lib "user32" Alias "WindowFromPoint" (ByVal xPoint As Long, ByVal yPoint As Long) As Long
        Public Declare Function GetCursorPos Lib "user32" Alias "GetCursorPos" (lpPoint As POINTAPI) As Long
        Public Declare Function GetWindowText Lib "user32" Alias "GetWindowTextA" (ByVal hwnd As Long, ByVal lpString As String, ByVal cch As Long) As Long
        Public Declare Function GetParent Lib "user32" Alias "GetParent" (ByVal hwnd As Long) As Long
        Public Declare Function GetClassName Lib "user32" Alias "GetClassNameA" (ByVal hwnd As Long, ByVal lpClassName As String, ByVal nMaxCount As Long) As Long
        Public Declare Function ChildWindowFromPoint Lib "user32" Alias "ChildWindowFromPoint" (ByVal hWndParent As Long, ByVal pt As POINTAPI) As Long*/

        [DllImport("User32.dll")]
        public static extern long WindowFromPoint(long x, long y);

        [DllImport("User32.dll")]
        public static extern long GetCursorPos(ref POINTAPI p);

        [DllImport("User32.dll")]
        public static extern long GetWindowTextA(long hwnd, ref string lpString, long cch);

        [DllImport("User32.dll")]
        public static extern long GetParent(long hwnd);

        [DllImport("User32.dll")]
        public static extern long ChildWindowFromPoint(long hWndParent, long x, long y);

        [DllImport("User32.dll")]
        public static extern long GetclassNameA(long hwnd, string lpClassName, long nMaxCount);
        [DllImport("user32.dll")]
        public static extern int GetForegroundWindow();
        [DllImport("user32.dll")]
        public static extern bool SetForegroundWindow(IntPtr hWnd);

        [DllImport("user32.dll")]
        public static extern int  GetWindowText(int hWnd, StringBuilder text, int count);

        public delegate bool EnumWindowCallback(int hwnd, int lParam); 
        
        [DllImport("user32.dll")] public static extern int EnumWindows(EnumWindowCallback callback, int y); 
        [DllImport("user32.dll")] public static extern int GetParent(int hWnd); 
        [DllImport("user32.dll")] public static extern long GetWindowLong(int hWnd, int nIndex); 
        [DllImport("user32.dll")] public static extern IntPtr GetClassLong(IntPtr hwnd, int nIndex);

        [DllImport("user32")]
        public static extern int GetClassName(int hwnd, StringBuilder lpClassName, int nMaxCount);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern IntPtr FindWindowEx(IntPtr hwndParent, IntPtr hwndChildAfter, string className, string lpszWindow);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern IntPtr GetWindow(IntPtr hWnd, uint uCmd);

        [DllImport("user32")]
        public static extern Int32 SendMessage(IntPtr hWnd, Int32 uMsg, IntPtr WParam, IntPtr LParam);

        public enum GetWindow_Cmd : uint
        {
            GW_HWNDFIRST = 0,
            GW_HWNDLAST = 1,
            GW_HWNDNEXT = 2,
            GW_HWNDPREV = 3,
            GW_OWNER = 4,
            GW_CHILD = 5,
            GW_ENABLEDPOPUP = 6
        }
        [DllImport("user32.dll")]
        public static extern void keybd_event(byte vk, byte scan, int flags, ref int extrainfo); 


        [DllImport("user32.dll")]
        public static extern bool ShowWindowAsync(IntPtr hWnd, int nCmdShow);

    }
}

