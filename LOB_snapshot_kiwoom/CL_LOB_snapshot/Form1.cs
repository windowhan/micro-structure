﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using Newtonsoft.Json;

namespace CL_LOB_snapshot
{
    public partial class Form1 : Form
    {
        public Dictionary<string, string> realtime_datatype;
        public Dictionary<string, string> realtime_datatype2;

        private int snapshot_hoga_count;
        private int snapshot_contract_count;
        private int my_handle;

        private bool b_firststart;
        public Form1()
        {
            InitializeComponent();
            //axKFOpenAPI1.CommConnect(1);

            this.b_firststart = true;
           
            // Initialize
            snapshot_hoga_count = 0;
            snapshot_contract_count = 0;

            realtime_datatype = new Dictionary<string, string>();
            realtime_datatype2 = new Dictionary<string, string>();

            realtime_datatype["매도수량5"] = "65";
            realtime_datatype["매도호가5"] = "45";
            realtime_datatype["매도건수5"] = "105";

            realtime_datatype["매도수량4"] = "64";
            realtime_datatype["매도호가4"] = "44";
            realtime_datatype["매도건수4"] = "104";

            realtime_datatype["매도수량3"] = "63";
            realtime_datatype["매도호가3"] = "43";
            realtime_datatype["매도건수3"] = "103";

            realtime_datatype["매도수량2"] = "62";
            realtime_datatype["매도호가2"] = "42";
            realtime_datatype["매도건수2"] = "102";

            realtime_datatype["매도수량1"] = "61";
            realtime_datatype["매도호가1"] = "41";
            realtime_datatype["매도건수1"] = "101";

            realtime_datatype["매수수량5"] = "75";
            realtime_datatype["매수호가5"] = "55";
            realtime_datatype["매수건수5"] = "115";

            realtime_datatype["매수수량4"] = "74";
            realtime_datatype["매수호가4"] = "54";
            realtime_datatype["매수건수4"] = "114";

            realtime_datatype["매수수량3"] = "73";
            realtime_datatype["매수호가3"] = "53";
            realtime_datatype["매수건수3"] = "113";

            realtime_datatype["매수수량2"] = "72";
            realtime_datatype["매수호가2"] = "52";
            realtime_datatype["매수건수2"] = "112";

            realtime_datatype["매수수량1"] = "71";
            realtime_datatype["매수호가1"] = "51";
            realtime_datatype["매수건수1"] = "111";

            realtime_datatype["매도호가총잔량"] = "121";
            realtime_datatype["매수호가총잔량"] = "125";
            realtime_datatype["호가시간"] = "21";

            realtime_datatype2["체결시간"] = "20";
            realtime_datatype2["현재가"] = "10";
            realtime_datatype2["대비기호"] = "25";
            realtime_datatype2["전일대비"] = "11";
            realtime_datatype2["등락율"] = "12";
            realtime_datatype2["체결량"] = "15";
        }

        private void axKFOpenAPI1_OnReceiveRealData(object sender, AxKFOpenAPILib._DKFOpenAPIEvents_OnReceiveRealDataEvent e)
        {
            string output = "";
            int hour_value = DateTime.Now.Hour;

            Dictionary<string, string> output_dict = new Dictionary<string, string>();
            if ((e.sRealType.Trim() == "해외선물시세") || (e.sRealType.Trim() == "해외옵션시세"))
            {
                output_dict["시간"] = DateTime.Now.ToString("HH:mm:ss.ffffff");
                output_dict["체결시간"] = axKFOpenAPI1.GetCommRealData(e.sJongmokCode, int.Parse(realtime_datatype2["체결시간"]));
                output_dict["현재가"] = axKFOpenAPI1.GetCommRealData(e.sJongmokCode, int.Parse(realtime_datatype2["현재가"]));
                output_dict["대비기호"] = axKFOpenAPI1.GetCommRealData(e.sJongmokCode, int.Parse(realtime_datatype2["대비기호"]));
                output_dict["체결량"] = axKFOpenAPI1.GetCommRealData(e.sJongmokCode, int.Parse(realtime_datatype2["체결량"]));
                output = JsonConvert.SerializeObject(output_dict, Formatting.None);
                snapshot_contract_count++;
                snapshot_contract_count_label.Text = snapshot_contract_count.ToString();

                if(hour_value > -1 && hour_value < 7)
                    System.IO.File.AppendAllText("./" + code_text.Text + "_" + DateTime.Now.AddDays(-1).ToString("yyyy/MM/dd") + "_contract.txt", output + "\r\n", Encoding.Unicode);
                else
                    System.IO.File.AppendAllText("./" + code_text.Text + "_" + DateTime.Now.ToString("yyyy/MM/dd") + "_contract.txt", output + "\r\n", Encoding.Unicode);
            }

            else if ((e.sRealType.Trim() == "해외선물호가") || (e.sRealType.Trim() == "해외옵션호가"))
            {
                output_dict["시간"] = DateTime.Now.ToString("HH:mm:ss.ffffff");
                output_dict["매도수량5"] = axKFOpenAPI1.GetCommRealData(e.sJongmokCode, int.Parse(realtime_datatype["매도수량5"]));
                output_dict["매도호가5"] = axKFOpenAPI1.GetCommRealData(e.sJongmokCode, int.Parse(realtime_datatype["매도호가5"]));
                output_dict["매도건수5"] = axKFOpenAPI1.GetCommRealData(e.sJongmokCode, int.Parse(realtime_datatype["매도건수5"]));

                output_dict["매도수량4"] = axKFOpenAPI1.GetCommRealData(e.sJongmokCode, int.Parse(realtime_datatype["매도수량4"]));
                output_dict["매도호가4"] = axKFOpenAPI1.GetCommRealData(e.sJongmokCode, int.Parse(realtime_datatype["매도호가4"]));
                output_dict["매도건수4"] = axKFOpenAPI1.GetCommRealData(e.sJongmokCode, int.Parse(realtime_datatype["매도건수4"]));

                output_dict["매도수량3"] = axKFOpenAPI1.GetCommRealData(e.sJongmokCode, int.Parse(realtime_datatype["매도수량3"]));
                output_dict["매도호가3"] = axKFOpenAPI1.GetCommRealData(e.sJongmokCode, int.Parse(realtime_datatype["매도호가3"]));
                output_dict["매도건수3"] = axKFOpenAPI1.GetCommRealData(e.sJongmokCode, int.Parse(realtime_datatype["매도건수3"]));

                output_dict["매도수량2"] = axKFOpenAPI1.GetCommRealData(e.sJongmokCode, int.Parse(realtime_datatype["매도수량2"]));
                output_dict["매도호가2"] = axKFOpenAPI1.GetCommRealData(e.sJongmokCode, int.Parse(realtime_datatype["매도호가2"]));
                output_dict["매도건수2"] = axKFOpenAPI1.GetCommRealData(e.sJongmokCode, int.Parse(realtime_datatype["매도건수2"]));

                output_dict["매도수량1"] = axKFOpenAPI1.GetCommRealData(e.sJongmokCode, int.Parse(realtime_datatype["매도수량1"]));
                output_dict["매도호가1"] = axKFOpenAPI1.GetCommRealData(e.sJongmokCode, int.Parse(realtime_datatype["매도호가1"]));
                output_dict["매도건수1"] = axKFOpenAPI1.GetCommRealData(e.sJongmokCode, int.Parse(realtime_datatype["매도건수1"]));

                output_dict["매수수량5"] = axKFOpenAPI1.GetCommRealData(e.sJongmokCode, int.Parse(realtime_datatype["매수수량5"]));
                output_dict["매수호가5"] = axKFOpenAPI1.GetCommRealData(e.sJongmokCode, int.Parse(realtime_datatype["매수호가5"]));
                output_dict["매수건수5"] = axKFOpenAPI1.GetCommRealData(e.sJongmokCode, int.Parse(realtime_datatype["매수건수5"]));

                output_dict["매수수량4"] = axKFOpenAPI1.GetCommRealData(e.sJongmokCode, int.Parse(realtime_datatype["매수수량4"]));
                output_dict["매수호가4"] = axKFOpenAPI1.GetCommRealData(e.sJongmokCode, int.Parse(realtime_datatype["매수호가4"]));
                output_dict["매수건수4"] = axKFOpenAPI1.GetCommRealData(e.sJongmokCode, int.Parse(realtime_datatype["매수건수4"]));

                output_dict["매수수량3"] = axKFOpenAPI1.GetCommRealData(e.sJongmokCode, int.Parse(realtime_datatype["매수수량3"]));
                output_dict["매수호가3"] = axKFOpenAPI1.GetCommRealData(e.sJongmokCode, int.Parse(realtime_datatype["매수호가3"]));
                output_dict["매수건수3"] = axKFOpenAPI1.GetCommRealData(e.sJongmokCode, int.Parse(realtime_datatype["매수건수3"]));

                output_dict["매수수량2"] = axKFOpenAPI1.GetCommRealData(e.sJongmokCode, int.Parse(realtime_datatype["매수수량2"]));
                output_dict["매수호가2"] = axKFOpenAPI1.GetCommRealData(e.sJongmokCode, int.Parse(realtime_datatype["매수호가2"]));
                output_dict["매수건수2"] = axKFOpenAPI1.GetCommRealData(e.sJongmokCode, int.Parse(realtime_datatype["매수건수2"]));

                output_dict["매수수량1"] = axKFOpenAPI1.GetCommRealData(e.sJongmokCode, int.Parse(realtime_datatype["매수수량1"]));
                output_dict["매수호가1"] = axKFOpenAPI1.GetCommRealData(e.sJongmokCode, int.Parse(realtime_datatype["매수호가1"]));
                output_dict["매수건수1"] = axKFOpenAPI1.GetCommRealData(e.sJongmokCode, int.Parse(realtime_datatype["매수건수1"]));

                output_dict["매수호가총잔량"] = axKFOpenAPI1.GetCommRealData(e.sJongmokCode, int.Parse(realtime_datatype["매수호가총잔량"]));
                output_dict["매도호가총잔량"] = axKFOpenAPI1.GetCommRealData(e.sJongmokCode, int.Parse(realtime_datatype["매도호가총잔량"]));


                snapshot_hoga_count++;
                snapshot_hoga_count_label.Text = snapshot_hoga_count.ToString();

                output = JsonConvert.SerializeObject(output_dict, Formatting.None);

                if (hour_value > -1 && hour_value < 7)
                    System.IO.File.AppendAllText("./" + code_text.Text + "_" + DateTime.Now.AddDays(-1).ToString("yyyy/MM/dd") + "_hoga.txt", output + "\r\n", Encoding.Unicode);
                
                else
                    System.IO.File.AppendAllText("./" + code_text.Text + "_" + DateTime.Now.ToString("yyyy/MM/dd") + "_hoga.txt", output + "\r\n", Encoding.Unicode);
                //redis.addList("current_hoga_" + database_text.Text, output);
            }
        }

        private void axKFOpenAPI1_OnReceiveTrData(object sender, AxKFOpenAPILib._DKFOpenAPIEvents_OnReceiveTrDataEvent e)
        {

        }

        private void snapshot_start_Click(object sender, EventArgs e)
        {
            this.b_firststart = false;
            autologin_timer.Start();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        public bool EnumWindowsProc(int hWnd, int lParam)
        {
            if (Win32Api.GetParent(hWnd) == this.my_handle)
            {
                StringBuilder sb = new StringBuilder(255);
                Win32Api.GetClassName(hWnd, sb, 255);
                if (sb.ToString().IndexOf("Edit") > -1)
                {
                    return true;
                }
                return true;
            }
            return true;
        }

        private void autologin_timer_Tick(object sender, EventArgs e)
        {
            int Info = 0;
            textBox1.Text = "[" + DateTime.Now.ToString("HH:mm:ss.ffffff") + "] autologin_timer 동작 \r\n" + textBox1.Text;
            textBox1.Text = "[" + DateTime.Now.ToString("HH:mm:ss.ffffff") + "] b_firststart : " + b_firststart.ToString() + " \r\n" + textBox1.Text;

            string form_title = this.Text;
            IntPtr form_hwnd = Win32Api.FindWindow(null, this.Text);

            /*if(!form_hwnd.Equals(IntPtr.Zero))
            {
                Win32Api.ShowWindowAsync(form_hwnd, 1); // SW_SHOWNORMAL
                Win32Api.SetForegroundWindow(form_hwnd);
            }*/

            if (this.axKFOpenAPI1.GetConnectState() == 0 && b_firststart == false)
            {

                this.b_firststart = true;
                axKFOpenAPI1.CommConnect(1);
                Thread.Sleep(2000);
                // test code
                StringBuilder buff = new StringBuilder(255);
                this.my_handle = Win32Api.GetForegroundWindow();
                Win32Api.GetWindowText(this.my_handle, buff, 255);
                int[] pw_arr = { 0x48, 0x4f, 0x4a, 0x55, 0x4e, 0x47, 0x31, 0x32 };
                textBox1.Text = "[" + DateTime.Now.ToString("HH:mm:ss.ffffff") + "] 로그인 로직 동작 \r\n" + textBox1.Text;
                if (buff.ToString().IndexOf("영웅문") > -1)
                {
                    //Win32Api.EnumWindowCallback callback = new Win32Api.EnumWindowCallback(this.EnumWindowsProc);
                    //Win32Api.EnumWindows(callback, 0);HOJUNG12
                    IntPtr id_hwnd = Win32Api.FindWindowEx(new IntPtr(this.my_handle), new IntPtr(0), "Edit", null);
                    IntPtr pw_hwnd = Win32Api.GetWindow(id_hwnd, 2);
                    IntPtr sign_hwnd = Win32Api.GetWindow(pw_hwnd, 2);

                    textBox1.Text += "id 텍스트 핸들 : " + id_hwnd.ToString("X") + "\r\n";
                    textBox1.Text += "pw 텍스트 핸들 : " + pw_hwnd.ToString("X") + "\r\n";

                    IntPtr login_hwnd = Win32Api.FindWindowEx(new IntPtr(this.my_handle), new IntPtr(0), "Button", null);
                    IntPtr id_ptr = Marshal.StringToHGlobalAnsi("windowha");
                    int result = 0;
                    result = Win32Api.SendMessage(id_hwnd, 0xc, new IntPtr(0), id_ptr); // #define WM_SETTEXT 0xC

                    textBox1.Text += "SetForegroundWindow called..." + "\r\n";
                    for (int i = 0; i < pw_arr.Length; i++)
                    {
                        Win32Api.SetForegroundWindow(pw_hwnd);
                        Win32Api.keybd_event(Convert.ToByte(pw_arr[i]), 0, 0, ref Info);

                        Thread.Sleep(50);
                        Win32Api.keybd_event(Convert.ToByte(pw_arr[i]), 0, 2, ref Info);
                        textBox1.Text += "눌린 키 : " + Convert.ToByte(pw_arr[i]).ToString() + "\r\n";
                        Thread.Sleep(50);
                    }
                    // Button Click
                    Win32Api.SendMessage(login_hwnd, 0x00F5, IntPtr.Zero, IntPtr.Zero);


                    this.autologin_timer.Stop();
                }
                
            }
        }

        public static class Constants
        {
            public const string TR_OPT10001 = "opt10001"; //현재가
            public const string TR_OPT10003 = "opt10005"; //관심종목
            public const string TR_OPC10001 = "opc10001"; //틱차트
            public const string TR_OPC10002 = "opc10002"; //분차트
            public const string TR_OPC10003 = "opc10003"; //일차트
            public const string TR_OPC10004 = "opc10004";//주차트
            public const string TR_OPC10005 = "opc10005"; //월차트
            public const string TR_OPW30005 = "opw30005"; //주문체결
            public const string TR_OPW30003 = "opw30003"; //잔고내역

            public const int OP_ERR_NONE = 0; //"정상처리" 
            public const int OP_ERR_NO_LOGIN = -1; //"미접속상태"	
            public const int OP_ERR_CONNECT = -101; //"서버 접속 실패" 
            public const int OP_ERR_VERSION = -102; //"버전처리가 실패하였습니다. 
            public const int OP_ERR_TRCODE = -103; //"TrCode가 존재하지 않습니다.”
            public const int OP_ERR_SISE_OVERFLOW = -200; //”시세과부하”
            public const int OP_ERR_ORDER_OVERFLOW = -201; //”주문과부하” 
            public const int OP_ERR_RQ_WRONG_INPUT = -202; //”조회입력값 오류”
            public const int OP_ERR_ORD_WRONG_INPUT = -300; //”주문입력값 오류”
            public const int OP_ERR_ORD_WRONG_ACCPWD = -301; //”계좌비밀번호를 입력하십시오.”
            public const int OP_ERR_ORD_WRONG_ACCNO = -302; //”타인 계좌를 사용할 수 없습니다.”
            public const int OP_ERR_ORD_WRONG_QTY200 = -303; //”경고-주문수량 200개 초과”
            public const int OP_ERR_ORD_WRONG_QTY400 = -304; //”제한-주문수량 400개 초과.”

            public const int DT_NONE = 0;		// 기본문자 형식
            public const int DT_DATE = 1;		// 일자 형식
            public const int DT_TIME = 2;		// 시간 형식
            public const int DT_NUMBER = 3;		// 콤마 숫자 형식
            public const int DT_ZERO_NUMBER = 4;		// 콤마 숫자(0표시) 형식
            public const int DT_SIGN = 5;		// 대비기호 형식
            public const int DT_NUMBER_NOCOMMA = 6;		// 콤마없는 숫자 형식
            public const int DT_ORDREG_GB = 7;		// 접수구분(1:접수, 2:확인, 3:체결, X:거부)
            public const int DT_ORDTYPE = 8;		// 주문유형(1:시장가, 2:지정가, 3:STOP, 4:STOPLIMIT)
            public const int DT_DOUBLE = 9;		// 실수값 처리( 소수점 2자리까지)
            public const int DT_ORDGUBUN = 10;		// 주문구분(1:매도, 2:매수)
            public const int DT_PRICE = 11;		// 가격표시

            public const int DT_LEFT = 16;
            public const int DT_RIGHT = 64;
            public const int DT_CENTER = 32;

            public const int UM_SCREEN_CLOSE = 1000;

        }

        private void timecheck_timer_Tick(object sender, EventArgs e) // start_timer
        {
            // 7시일 때 - API 시작
            string date = DateTime.Now.ToString("HHmmss");
            if(int.Parse(date)>070000)
            {
                textBox1.Text = "[" + DateTime.Now.ToString("HH:mm:ss.ffffff") + "] start_timer 동작 \r\n" + textBox1.Text;
                autologin_timer.Start();
                start_timer.Stop();
            }
        }

        private void end_timer_Tick(object sender, EventArgs e)
        {
            // 6시일 때 종료
            string date = DateTime.Now.ToString("HHmmss");
            if(date == "060000")
            {
                textBox1.Text = "[" + DateTime.Now.ToString("HH:mm:ss.ffffff") + "] end_timer 동작 \r\n" + textBox1.Text;
                this.axKFOpenAPI1.CommTerminate();
                start_timer.Start();
                end_timer.Stop();
            }
        }

        private void axKFOpenAPI1_OnEventConnect(object sender, AxKFOpenAPILib._DKFOpenAPIEvents_OnEventConnectEvent e)
        {
            if(e.nErrCode == 0)
            {
                textBox1.Text = "[" + DateTime.Now.ToString("HH:mm:ss.ffffff") + "] 로그인 완료!! \r\n" + textBox1.Text;
                string strRQName = "선물옵션현재가";
                string strTRCode = Constants.TR_OPT10001;
                axKFOpenAPI1.SetInputValue("종목코드", code_text.Text);

                int iRet = axKFOpenAPI1.CommRqData(strRQName, strTRCode, "", "13");
                if (iRet == 0) { }
                end_timer.Start();
            }

            else
            {
                b_firststart = false;
                this.autologin_timer.Start();
            }

            textBox1.Text = "[" + DateTime.Now.ToString("HH:mm:ss.ffffff") + "] Called axKFOpenAPI1_OnEventConnect... Code : " + e.nErrCode.ToString() + "\r\n" + textBox1.Text;
            //MessageBox.Show("OnEventConnect called...");
        }

        private void exit_btn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
