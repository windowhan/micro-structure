﻿namespace CL_LOB_snapshot
{
    partial class Form1
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다.
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.code_text = new System.Windows.Forms.TextBox();
            this.database_text = new System.Windows.Forms.TextBox();
            this.snapshot_start = new System.Windows.Forms.Button();
            this.snapshot_hoga_count_label = new System.Windows.Forms.Label();
            this.axKFOpenAPI1 = new AxKFOpenAPILib.AxKFOpenAPI();
            this.snapshot_contract_count_label = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.autologin_timer = new System.Windows.Forms.Timer(this.components);
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.start_timer = new System.Windows.Forms.Timer(this.components);
            this.end_timer = new System.Windows.Forms.Timer(this.components);
            this.exit_btn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.axKFOpenAPI1)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "종목코드";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 56);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 3;
            this.label3.Text = "연결종목";
            // 
            // code_text
            // 
            this.code_text.Location = new System.Drawing.Point(78, 26);
            this.code_text.Name = "code_text";
            this.code_text.Size = new System.Drawing.Size(233, 21);
            this.code_text.TabIndex = 4;
            // 
            // database_text
            // 
            this.database_text.Location = new System.Drawing.Point(78, 53);
            this.database_text.Name = "database_text";
            this.database_text.Size = new System.Drawing.Size(233, 21);
            this.database_text.TabIndex = 6;
            // 
            // snapshot_start
            // 
            this.snapshot_start.Location = new System.Drawing.Point(19, 97);
            this.snapshot_start.Name = "snapshot_start";
            this.snapshot_start.Size = new System.Drawing.Size(292, 23);
            this.snapshot_start.TabIndex = 7;
            this.snapshot_start.Text = "시작";
            this.snapshot_start.UseVisualStyleBackColor = true;
            this.snapshot_start.Click += new System.EventHandler(this.snapshot_start_Click);
            // 
            // snapshot_hoga_count_label
            // 
            this.snapshot_hoga_count_label.AutoSize = true;
            this.snapshot_hoga_count_label.Location = new System.Drawing.Point(12, 204);
            this.snapshot_hoga_count_label.Name = "snapshot_hoga_count_label";
            this.snapshot_hoga_count_label.Size = new System.Drawing.Size(38, 12);
            this.snapshot_hoga_count_label.TabIndex = 8;
            this.snapshot_hoga_count_label.Text = "label4";
            // 
            // axKFOpenAPI1
            // 
            this.axKFOpenAPI1.Enabled = true;
            this.axKFOpenAPI1.Location = new System.Drawing.Point(242, 204);
            this.axKFOpenAPI1.Name = "axKFOpenAPI1";
            this.axKFOpenAPI1.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axKFOpenAPI1.OcxState")));
            this.axKFOpenAPI1.Size = new System.Drawing.Size(100, 50);
            this.axKFOpenAPI1.TabIndex = 0;
            this.axKFOpenAPI1.Visible = false;
            this.axKFOpenAPI1.OnReceiveTrData += new AxKFOpenAPILib._DKFOpenAPIEvents_OnReceiveTrDataEventHandler(this.axKFOpenAPI1_OnReceiveTrData);
            this.axKFOpenAPI1.OnReceiveRealData += new AxKFOpenAPILib._DKFOpenAPIEvents_OnReceiveRealDataEventHandler(this.axKFOpenAPI1_OnReceiveRealData);
            this.axKFOpenAPI1.OnEventConnect += new AxKFOpenAPILib._DKFOpenAPIEvents_OnEventConnectEventHandler(this.axKFOpenAPI1_OnEventConnect);
            // 
            // snapshot_contract_count_label
            // 
            this.snapshot_contract_count_label.AutoSize = true;
            this.snapshot_contract_count_label.Location = new System.Drawing.Point(12, 232);
            this.snapshot_contract_count_label.Name = "snapshot_contract_count_label";
            this.snapshot_contract_count_label.Size = new System.Drawing.Size(38, 12);
            this.snapshot_contract_count_label.TabIndex = 10;
            this.snapshot_contract_count_label.Text = "label4";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.exit_btn);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.code_text);
            this.groupBox3.Controls.Add(this.snapshot_start);
            this.groupBox3.Controls.Add(this.database_text);
            this.groupBox3.Location = new System.Drawing.Point(14, 17);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(330, 181);
            this.groupBox3.TabIndex = 13;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "설정";
            // 
            // autologin_timer
            // 
            this.autologin_timer.Interval = 3000;
            this.autologin_timer.Tick += new System.EventHandler(this.autologin_timer_Tick);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(14, 260);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(330, 170);
            this.textBox1.TabIndex = 8;
            // 
            // start_timer
            // 
            this.start_timer.Interval = 1000;
            this.start_timer.Tick += new System.EventHandler(this.timecheck_timer_Tick);
            // 
            // end_timer
            // 
            this.end_timer.Interval = 1000;
            this.end_timer.Tick += new System.EventHandler(this.end_timer_Tick);
            // 
            // exit_btn
            // 
            this.exit_btn.Location = new System.Drawing.Point(19, 126);
            this.exit_btn.Name = "exit_btn";
            this.exit_btn.Size = new System.Drawing.Size(292, 23);
            this.exit_btn.TabIndex = 8;
            this.exit_btn.Text = "나가기";
            this.exit_btn.UseVisualStyleBackColor = true;
            this.exit_btn.Click += new System.EventHandler(this.exit_btn_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(354, 442);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.axKFOpenAPI1);
            this.Controls.Add(this.snapshot_contract_count_label);
            this.Controls.Add(this.snapshot_hoga_count_label);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Form1";
            this.Text = "호가창 스냅샷";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.axKFOpenAPI1)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private AxKFOpenAPILib.AxKFOpenAPI axKFOpenAPI1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox code_text;
        private System.Windows.Forms.TextBox database_text;
        private System.Windows.Forms.Button snapshot_start;
        private System.Windows.Forms.Label snapshot_hoga_count_label;
        private System.Windows.Forms.Label snapshot_contract_count_label;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Timer autologin_timer;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Timer start_timer;
        private System.Windows.Forms.Timer end_timer;
        private System.Windows.Forms.Button exit_btn;
    }
}

