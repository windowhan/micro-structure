﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace 조건검색식_로거
{
    public partial class Form1 : Form
    {
        private List<ConditionInfo> conditionList;

        public Form1()
        {
            InitializeComponent();
            this.axKHOpenAPI1.Visible = false;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.groupBox1.Enabled = false;
            this.list_combobox.DropDownStyle = ComboBoxStyle.DropDownList;
            int result = this.axKHOpenAPI1.CommConnect();
            if (result != 0)
                MessageBox.Show("Login창을 열지 못했습니다.");
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {
        }

        private void axKHOpenAPI1_OnEventConnect(object sender, AxKHOpenAPILib._DKHOpenAPIEvents_OnEventConnectEvent e)
        {
            // 로그인 관련 이벤트
            if(e.nErrCode==0)
            {
                this.groupBox1.Enabled = true;
                // 조건식 불러오기
                this.axKHOpenAPI1.GetConditionLoad();
            }

            else
            {
                MessageBox.Show("로그인 실패");
                Application.Exit();
            }
        }

        private void axKHOpenAPI1_OnReceiveTrData(object sender, AxKHOpenAPILib._DKHOpenAPIEvents_OnReceiveTrDataEvent e)
        {

        }

        private void axKHOpenAPI1_OnReceiveRealData(object sender, AxKHOpenAPILib._DKHOpenAPIEvents_OnReceiveRealDataEvent e)
        {
            if(e.sRealType == "주식체결")
            {
                string current_price = axKHOpenAPI1.GetCommRealData(e.sRealType, 10).Trim();
                string updown_rate = axKHOpenAPI1.GetCommRealData(e.sRealType, 12).Trim();
                string now_time = DateTime.Now.ToString("HH:mm:ss.ffffff");

                for (int i = 0; i < listView1.Items.Count; i++)
                {
                    // // 출현시간, 종목이름(번호), 출현당시가격, 출현당시등락율, 현재가격, 현재등락율, 최고가격, 최고가도달시간, 이탈시간
                    if (listView1.Items[i].SubItems[2].Text == "X")
                    {
                        listView1.Items[i].SubItems[2].Text = current_price;
                        listView1.Items[i].SubItems[3].Text = updown_rate;
                    }

                    listView1.Items[i].SubItems[4].Text = current_price;
                    listView1.Items[i].SubItems[5].Text = updown_rate;

                    if(listView1.Items[i].SubItems[6].Text == "X" || int.Parse(listView1.Items[i].SubItems[6].Text) <  int.Parse(current_price))
                    {
                        listView1.Items[i].SubItems[6].Text = current_price;
                        listView1.Items[i].SubItems[7].Text = now_time;
                    }
                }
            }
        }

        private void axKHOpenAPI1_OnReceiveTrCondition(object sender, AxKHOpenAPILib._DKHOpenAPIEvents_OnReceiveTrConditionEvent e)
        {

        }

        private void axKHOpenAPI1_OnReceiveConditionVer(object sender, AxKHOpenAPILib._DKHOpenAPIEvents_OnReceiveConditionVerEvent e)
        {
            this.conditionList = new List<ConditionInfo>();

            string conditionNameList = this.axKHOpenAPI1.GetConditionNameList();
            string[] conditionNameArray = conditionNameList.Split(';');

            for(int i=0;i<conditionNameArray.Length;i++)
            {
                string[] conditionInfo = conditionNameArray[i].Split('^');
                if(conditionInfo.Length==2)
                {
                    conditionList.Add(new ConditionInfo() { 조건식번호=int.Parse(conditionInfo[0].Trim()), 조건식이름=conditionInfo[1].Trim()});
                    list_combobox.Items.Add("[" + conditionList[conditionList.Count - 1].조건식번호.ToString() + "] " + conditionList[conditionList.Count - 1].조건식이름);
                }
            }
        }


        private void refresh_btn_Click(object sender, EventArgs e)
        {
            this.conditionList.Clear();
            this.list_combobox.Items.Clear();
            this.axKHOpenAPI1.GetConditionLoad();
        }

        private void set_search_expression_btn_Click(object sender, EventArgs e)
        {
            /*
             * 
             * LPCTSTR strScrNo : 화면번호
             * LPCTSTR strConditionName : 조건명
             * int nIndex : 조건명인덱스
             * int nSearch : 조회구분(0:일반조회, 1:실시간조회, 2:연속조회)
             */
            if (set_search_expression_btn.Text == "시작")
            {
                int result = this.axKHOpenAPI1.SendCondition("13", conditionList[list_combobox.SelectedIndex].조건식이름, conditionList[list_combobox.SelectedIndex].조건식번호, 1);
                if (result == 0)
                {
                    MessageBox.Show("실시간 조건검색식 사용에 실패했습니다.");
                }

                else
                {
                    set_search_expression_btn.Text = "중지";
                    list_combobox.Enabled = false;
                    refresh_btn.Enabled = false;
                }
            }

            else
            {
                this.axKHOpenAPI1.SendConditionStop("13", conditionList[list_combobox.SelectedIndex].조건식이름, conditionList[list_combobox.SelectedIndex].조건식번호);
                set_search_expression_btn.Text = "시작";
                list_combobox.Enabled = true;
                refresh_btn.Enabled = true;
                
            }
        }

        private void axKHOpenAPI1_OnReceiveRealCondition(object sender, AxKHOpenAPILib._DKHOpenAPIEvents_OnReceiveRealConditionEvent e)
        {
            // 출현시간, 종목이름(번호), 출현당시가격, 출현당시등락율, 현재가격, 현재등락율, 최고가격, 최고가도달시간, 이탈시간
            if(e.strType == "I") // 종목 편입 
            {
                string stockName = axKHOpenAPI1.GetMasterCodeName(e.sTrCode);

                string uptime = DateTime.Now.ToString("HH:mm:ss.ffffff");
                string stockIdx = e.sTrCode;

                listView1.Items.Add(new ListViewItem(new string[]{uptime, stockName + "(" + stockIdx + ")", "X", "X", "X", "X", "X", "X", "X"}));

                // 실시간 시세 요청
                axKHOpenAPI1.SetInputValue("종목코드", stockIdx);
                int ret = axKHOpenAPI1.CommRqData("주식기본정보", "OPT10001", 0, "1001");
                if (ret != 0)
                    MessageBox.Show("현재가 요청 오류");
            }

            else if(e.strType == "D") // 종목 이탈
            {
                string stockName = axKHOpenAPI1.GetMasterCodeName(e.sTrCode);
                string downtime = DateTime.Now.ToString("HH:mm:ss.ffffff");
                string stockIdx = e.sTrCode;
                string findData = stockName + "(" + stockIdx + ")";


                for(int i=0;i<listView1.Items.Count;i++)
                {
                    if(listView1.Items[i].SubItems[1].Text == findData && listView1.Items[i].SubItems[8].Text == "X")
                    {
                        listView1.Items[i].SubItems[8].Text = downtime;
                    }
                }
            }
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void price_update_timer_Tick(object sender, EventArgs e)
        {

        }
    }

    class ConditionInfo
    {
        public int 조건식번호 { get; set; }
        public string 조건식이름 { get; set; }
    }

    class StockItemInfo
    {
        public string 종목명 { get; set; }
        public string 현재가 { get; set; }
        public string 전일대비 { get; set; }
        public string 등락률 { get; set; }
        public string 거래량 { get; set; }
        public string 시가 { get; set; }
        public string 고가 { get; set; }
        public string 저가 { get; set; }

    }
}
